<?php

return [
	'table' => 'oauth_identities',
	'providers' => [
		'facebook' => [
			'client_id' => '453707734808729',
			'client_secret' => 'f77d547e80360b8f235a6d3b0e373440',
			'redirect_uri' => 'http://aluno.dancesempre.com/facebook/login',
			'scope' => [],
		],
		// 'google' => [
		// 	'client_id' => '12345678',
		// 	'client_secret' => 'y0ur53cr374ppk3y',
		// 	'redirect_uri' => 'https://example.com/your/google/redirect',
		// 	'scope' => [],
		// ],
		// 'github' => [
		// 	'client_id' => '12345678',
		// 	'client_secret' => 'y0ur53cr374ppk3y',
		// 	'redirect_uri' => 'https://example.com/your/github/redirect',
		// 	'scope' => [],
		// ],
		// 'linkedin' => [
		// 	'client_id' => '12345678',
		// 	'client_secret' => 'y0ur53cr374ppk3y',
		// 	'redirect_uri' => 'https://example.com/your/linkedin/redirect',
		// 	'scope' => [],
		// ],
		// 'instagram' => [
		// 	'client_id' => '12345678',
		// 	'client_secret' => 'y0ur53cr374ppk3y',
		// 	'redirect_uri' => 'https://example.com/your/instagram/redirect',
		// 	'scope' => [],
		// ],
		// 'soundcloud' => [
		// 	'client_id' => '12345678',
		// 	'client_secret' => 'y0ur53cr374ppk3y',
		// 	'redirect_uri' => 'https://example.com/your/soundcloud/redirect',
		// 	'scope' => [],
		// ],
	],
];
