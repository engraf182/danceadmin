<!DOCTYPE html>
<html lang="pt">
<head>
	<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-M8GP4VD');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') Admin</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/style_greg.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
    <script src="{{ asset('assets/ckeditor/ckeditor.js') }}"></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <link href="{{ asset('assets/dynatable/jquery.dynatable.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/dynatable/jquery.dynatable-bootstrap.js') }}"></script>
    <link href="//oss.maxcdn.com/semantic-ui/2.0.8/components/item.min.css" rel="stylesheet">
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-XXXX"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				{{-- <a class="navbar-brand" href="/">Admin</a> --}}
				<a class="navbar-brand" style="padding: 10px;" href="/"><img height="30px" src="http://dancesempre.com/images/dance-sempre.png"></a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				@is('admin')
					<ul class="nav navbar-nav">
						<li><a href="{{ url('/admin') }}"><i class="fa fa-home"></i> Home</a></li>
	                    <li><a href="{{ route('banners') }}"><i class="fa fa-bookmark-o"></i> Banners</a></li>
	                    <li><a href="{{ route('professores') }}"><i class="fa fa-graduation-cap"></i> Professores</a></li>
	                    <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> Alunos <span class="caret"></span></a>
	                        <ul class="dropdown-menu" role="menu">
	                        	<li><a href="{{ route('alunos.create') }}"> Cadastrar Aluno</a></li>
	                        	<li><a href="{{ route('alunos') }}"> Alunos Cadastrados</a></li>
	                        	<li><a href="{{ route('alunos.teste') }}"> Alunos Sistema Antigo</a></li>
	                        </ul>
	                    </li>
	                    <li><a href="{{ route('modalidades') }}"><i class="fa fa-shield"></i> Modalidades</a></li>
	                    <li><a href="{{ route('paginas') }}"><i class="fa fa-file-code-o"></i> Páginas</a></li>
	                    <li><a href="{{ route('turmas') }}"><i class="fa fa-puzzle-piece"></i> Turmas</a></li>
	                    <li><a href="{{ route('album') }}"><i class="fa fa-picture-o"></i> Fotos</a></li>
	                    <li><a href="{{ route('alunos.cadastros') }}"><i class="fa fa-asterisk"></i> Cadastros</a></li>
					</ul>
				@endis
				@is('alunos')
					<ul class="nav navbar-nav">
						<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
	                    <li><a href="#"><i class="fa fa-bookmark-o"></i> Créditos</a></li>
	                    <li><a href="#"><i class="fa fa-bookmark-o"></i> Meus recibos</a></li>
	                    <li><a href="#"><i class="fa fa-puzzle-piece"></i> Turmas que frequentei</a></li>
					</ul>
				@endis

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}"><i class="fa fa-sign-in"></i> Login</a></li>
						{{-- <li><a href="{{ url('/auth/register') }}">Register</a></li> --}}
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<div class="row">
									<div class="col-xs-12 col-md-3">
										@if(Auth::user()->avatar = 'images/avatar.jpg')
											<img width="30" src="{{ url(Auth::user()->avatar) }}" alt="{{ Auth::user()->name }}" class="img-circle center-block">
										@else
											<img width="30" src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" class="img-circle center-block">
										@endif
									</div>
									<div class="col-xs-12 col-md-9">
										{{ Auth::user()->name }} <span class="caret"></span>
									</div>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out"></i> Sair</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('flash_message'))
                    <div class="alert {{ Session::get('flash_type') }} alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

            </div>
        </div>
    </div>
	@yield('content')
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

    <script type="text/javascript">
		// DYNATABLE //
        $('.table').dynatable();

        // PAGESPY //
		var str=location.href.toLowerCase();

		$(".navbar li a").each(function() {
			if (str.indexOf(this.href.toLowerCase()) > -1) {
				$(".navbar li.active").removeClass("active");
				$(this).parent().addClass("active");
				var parentDropDown = $(this).closest('.dropdown');
				if(parentDropDown !== undefined){
					parentDropDown.addClass('active');
				}
			}
		});
    </script>
    @yield('footer')
	<br/><br/>
</body>
</html>
