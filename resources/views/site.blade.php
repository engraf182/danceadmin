<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dance Sempre - Escola de dança de salão</title>
	<meta name="author" content="Pixel3 Solutions">
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>

	<!-- Bootstrap core JavaScript -->
	<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
	<!-- FlexSlider -->
	<link rel="stylesheet" href="{{ asset('assets/flexslider/flexslider.css') }}" type="text/css" media="screen" />
	<link rel="stylesheet" href="{{ asset('assets/flexslider/custom.css') }}">
	<!-- Common head includes -->
	<link href="{{ asset('/css/custom_bootstrap.css') }}" rel="stylesheet">
	<!-- Page-Level CSS -->
	<link href="{{ asset('/css/main.css') }}" rel="stylesheet">
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	@yield('head')
</head>

<body role="document">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M8GP4VD"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
		<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M8GP4VD');</script>
	<!-- End Google Tag Manager -->
	<!-- Fixed navbar -->
	<nav class="navbar navbar-default navbar-fixed-top navbar-wide" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
					<span class="sr-only">Abrir menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../home/">
					<img class="hidden-md" src="{{ asset('images/dance-sempre.png') }}" alt="Dance Sempre">
				</a>
			</div>

			<div class="collapse navbar-collapse" id="navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="../institucional">Institucional</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Cursos <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="../servicos">Serviços</a></li>
							<li class="divider"></li>
							<li><a href="../metodologia">Metodologia</a></li>
							<li><a href="../estrutura">Estrutura</a></li>
							<li class="divider"></li>
							<!-- <li><a href="#">Aula Experimental</a></li> -->
						</ul>
					</li>
					<li><a href="../horarios">Horários</a></li>
					<li><a href="../danca-dos-noivos">Dança dos Noivos</a></li>
					<li><a href="../cadastro">Cadastro</a></li>
					<li><a href="../valores">Valores</a></li>
					<li><a href="../modalidades">Modalidades</a></li>
					<li><a href="../fotos">Fotos</a></li>
					<!-- <li><a href="#">Área do Aluno</a></li>
					<li><a href="../parceiros">Parceiros</a></li> -->
					<!-- <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Arquivos <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="../fotos">Fotos</a></li>
							<li><a href="../videos">Vídeos</a></li>
						</ul>
					</li> -->
					<li><a href="http://blog.dancesempre.com">Blog</a></li>
					<li><a href="../contato">Contato</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>

	@yield('botao')

	<main>
		<!-- Wrapper -->
		<div class="wrapper">
			@yield('content')
		</div><!-- /.Wrapper -->

		<div class="container">
			<div class="col-xs-12">
				<hr>
					<div class="table-responsive" style="overflow: hidden;">
						<iframe scrolling="no" frameborder="1" style="border:none; width:100%; height:80px" src="https://www.facebook.com/plugins/like.php?href=https://www.facebook.com/dancesempre"></iframe>
					</div>
				<hr>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<footer>
					<div class="container" style="padding-bottom: 20px;">
						<div class="row">
							<div class="col-xs-12 col-md-2 text-center">
								<img id="footer-brand" src="{{ asset('images/dance-sempre-footer.png') }}" alt="Dance Sempre">
							</div><!-- ./col-xs-12 -->
							<div class="hidden-12 col-md-9 col-md-offset-1">
								<div class="row hidden-xs hidden-sm">
									<div class="col-xs-3">
										<h4>Cursos</h4>
										<ul>
											<a href="../servicos">
												<li>Serviços</li>
											</a>
											<a href="../metodologia">
												<li>Metodologia</li>
											</a>
											<a href="../estrutura">
												<li>Estrutura</li>
											</a>
											<!-- <a href="#">
												<li>Aula Experimental</li>
											</a> -->
										</ul>
									</div><!-- ./col-xs-3 -->
									<div class="col-xs-3">
										<a href="../institucional"><h4>Institucional</h4></a>
										<a href="../horarios"><h4>Horários</h4></a>
										<a href="../valores"><h4>Valores</h4></a>
										<!-- <a href="#"><h4>Mídia</h4></a>
										<ul>
											<a href="#">
												<li>Fotos</li>
											</a>
											<a href="#">
												<li>Vídeos</li>
											</a>
										</ul> -->
									</div><!-- ./col-xs-3 -->
									<div class="col-xs-3">
										<a href="../blog"><h4>Blog</h4></a>
										<a href="../modalidades"><h4>Modalidades</h4></a>
										<!-- <a href="#"><h4>Área do Aluno</h4></a>
										<a href="#"><h4>Área do Professor</h4></a> -->
										<a href="../contato"><h4>Contato</h4></a>
									</div><!-- ./col-xs-3 -->
								</div><!-- ./row -->
								<div class="row">
									<div class="col-xs-12 col-lg-7">
										<address>
											<p>Praça Osório, 431, Último andar - Centro | Curitiba - PR<br/>Fone: 41 3023-1553 | venhapara@dancesempre.com</p>
										</address>
									</div><!-- ./col-xs-12 -->
									<div id="sociais" class="col-xs-12 col-lg-3 text-center">
										<div class="row">
											<div class="col-xs-12">
												<a href="http://www.facebook.com/dancesempre" target="_blank">
													<img src="{{ asset('images/icon-facebook.png') }}" alt="Facebook">
												</a>
												<a href="https://twitter.com/Dancesempre" target="_blank">
													<img src="{{ asset('images/icon-twitter.png') }}" alt="Twitter">
												</a>
												<a href="#" target="_blank">
													<img src="{{ asset('images/icon-instagram.png') }}" alt="Instagram">
												</a>
												<a href="https://www.youtube.com/user/sempredancesempre#p/a" target="_blank">
													<img src="{{ asset('images/icon-youtube.png') }}" alt="Youtube">
												</a>
											</div>
										</div>
									</div><!-- ./col-xs-12 -->
									<div class="col-xs-12 col-lg-2 text-center">
										<a href="http://www.pixel3.solutions" target="_blank" title="Desenvolvido por Pixel 3 Solutions"><img width="50" class="center-block" title="Pixel3 Solutions" title="Pixel3 Solutions" src="{{ asset('images/pixel.png') }}"></a>
									</div>
								</div><!-- ./row -->
							</div><!-- ./col-xs-12 -->
						</div><!-- ./row -->
					</div><!-- ./container -->
				</footer>
			</div><!-- ./row -->
		</div><!-- ./container-fluid -->

	</main>
	<!-- FlexSlider -->
	<script type="text/javascript" src="{{ asset('assets/flexslider/flexslider-min.js') }}"></script>
	<!-- ./ FlexSlider -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
	<script type="text/javascript">
        // PAGESPY //
		var str=location.href.toLowerCase();

		$(".navbar li a").each(function() {
			if (str.indexOf(this.href.toLowerCase()) > -1) {
				$(".navbar li.active").removeClass("active");
				$(this).parent().addClass("active");
				var parentDropDown = $(this).closest('.dropdown');
				if(parentDropDown !== undefined){
					parentDropDown.addClass('active');
				}
			}
		});
		$(window).load(function(){
		  $('.flexslider').flexslider({
		    animation: "slide",
		    start: function(slider){
		      $('body').removeClass('loading');
		    }
		  });
		});
    </script>
    <!-- Optional FlexSlider Additions -->
    <script src="{{ asset('assets/flexslider/demo/js/jquery.easing.js') }}"></script>
    <script src="{{ asset('assets/flexslider/demo/js/jquery.mousewheel.js') }}"></script>
    @yield('js')
</body>
</html>
