@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Adicionar Turma</h1>
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::open(['route'=>['turmas.store']]) !!}
                    @include('turmas.form', ['text_button' => 'Adicionar turma'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection