<div class="row">
    <div class="col-xs-12 col-md-4">
        <div class="form-group">
            <label for="professor">Modalidade</label>
            {!! Form::select('modalidade', $modalidades, null, array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="form-group">
            <label for="professor">Professor</label>
            {!! Form::select('professor', $professores, null, array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="form-group">
            <label for="professor">Nível</label>
            {!! Form::select('nivel', [
                'Iniciante' => 'Iniciante',
                'Básico' => 'Básico',
                'Intermediário' => 'Intermediário',
                'Avançado' => 'Avançado'], null, ['class' => 'form-control']
            ) !!}
        </div>
    </div>

    <div class="col-xs-12">
        <div class="form-group">
            <label for="dias_semana">Dias de Aula</label><br/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <label>
                Segunda
                {!! Form::checkbox('diasSemana[]','Segunda') !!}
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label>
                Terça
                {!! Form::checkbox('diasSemana[]','Terça') !!}
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label>
                Quarta
                {!! Form::checkbox('diasSemana[]','Quarta') !!}
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label>
                Quinta
                {!! Form::checkbox('diasSemana[]','Quinta') !!}
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label>
                Sexta
                {!! Form::checkbox('diasSemana[]','Sexta') !!}
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label>
                Sábado
                {!! Form::checkbox('diasSemana[]','Sábado') !!}
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label>
                Domingo
                {!! Form::checkbox('diasSemana[]','Domingo') !!}
            </label>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="form-group">
            {!! Form::label('horarioInicio', 'Horário Inicial') !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-clock-o fa-fw"></i></span>
                {!! Form::text('horarioInicio', null, ['class'=>'form-control', 'placeholder'=>'20h30', 'data-mask'=>'99h99']) !!}
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="form-group">
            {!! Form::label('horarioFim', 'Horário Final') !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-clock-o fa-fw"></i></span>
                {!! Form::text('horarioFim', null, ['class'=>'form-control', 'placeholder'=>'20h30', 'data-mask'=>'99h99']) !!}
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="form-group">
            {!! Form::label('credioAula', 'Crédito por aula') !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-usd fa-fw"></i></span>
                {!! Form::text('credioAula', null, ['class'=>'form-control', 'placeholder'=>'1.5 ou 1.0', 'data-mask'=>'9.9']) !!}
            </div>
        </div>
    </div>
</div>
<hr/>
<div class="form-group">
    {!! Form::submit($text_button, ['class'=>'btn btn-primary pull-right']) !!}
</div>