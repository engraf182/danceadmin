@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Turmas</h1>
                <a href="{{ route('turmas.create') }}" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-plus lg"></i></span>Adicionar turma</a><br/><br/>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Modalidade</th>
                                <th>Professor</th>
                                <th>Horário</th>
                                <th>Nível</th>
                                <th>Dias de aula</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($turmas as $turma)
                                <?php $dias = explode(",", $turma->diasSemana); ?>
                                <tr>
                                    <td>{{ $turma->modalidades->modalidade }}</td>
                                    <td>{{ $turma->professores->nome_prof }}</td>
                                    <td>{{ $turma->horarioInicio }} às {{ $turma->horarioFim }}</td>
                                    <td>{{ $turma->nivel }}</td>
                                    <td>
                                        @foreach($dias as $dia)
                                            <span class="label label-info">{{ $dia }}</span>
                                        @endforeach
                                    </td>
                                    <td>
                                        <a class="btn btn-warning" title="Veja os dados da turma de {{ $turma->modalidades->modalidade }} ({{ $turma->diasSemana }})" href="{{ route('turmas.view',['id'=>$turma->id]) }}"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-success" href="{{ route('turmas.edit',['id'=>$turma->id]) }}"><i class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-danger" href="{{ route('turmas.destroy',['id'=>$turma->id]) }}" onclick="return confirm('Deseja deletar esta turma ?')"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

@stop