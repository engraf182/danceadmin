@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Editar Turma</h1>
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::model($turma, ['route'=>['turmas.update', $turma->id], 'method'=>'put']) !!}
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                            <label for="professor">Modalidade</label>
                            {!! Form::select('modalidade', $modalidades, $turma->modalidades_id, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                            <label for="professor">Professor</label>
                            {!! Form::select('professor', $professores, $turma->professores_id, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                            <label for="professor">Nível</label>
                            {!! Form::select('nivel', [
                            'Iniciante' => 'Iniciante',
                            'Básico' => 'Básico',
                            'Intermediário' => 'Intermediário',
                            'Avançado' => 'Avançado'], null, ['class' => 'form-control']
                            ) !!}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="dias_semana">Dias de Aula</label><br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php $dias = explode(",", $turma->diasSemana);?>
                            <label>
                                Segunda
                                @if(in_array("Segunda", $dias))
                                    {!! Form::checkbox('dias[]','Segunda', true) !!}
                                @else
                                    {!! Form::checkbox('dias[]','Segunda')  !!}
                                @endif
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label>
                                Terça
                                @if(in_array("Terça", $dias))
                                    {!! Form::checkbox('dias[]','Terça', true) !!}
                                @else
                                    {!! Form::checkbox('dias[]','Terça')  !!}
                                @endif
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label>
                                Quarta
                                @if(in_array("Quarta", $dias))
                                    {!! Form::checkbox('dias[]','Quarta', true) !!}
                                @else
                                    {!! Form::checkbox('dias[]','Quarta')  !!}
                                @endif

                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label>
                                Quinta
                                @if(in_array("Quinta", $dias))
                                    {!! Form::checkbox('dias[]','Quinta', true) !!}
                                @else
                                    {!! Form::checkbox('dias[]','Quinta')  !!}
                                @endif
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label>
                                Sexta
                                @if(in_array("Sexta", $dias))
                                    {!! Form::checkbox('dias[]','Sexta', true) !!}
                                @else
                                    {!! Form::checkbox('dias[]','Sexta')  !!}
                                @endif
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label>
                                Sábado
                                @if(in_array("Sábado", $dias))
                                    {!! Form::checkbox('dias[]','Sábado', true) !!}
                                @else
                                    {!! Form::checkbox('dias[]','Sábado')  !!}
                                @endif
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label>
                                Domingo
                                @if(in_array("Domingo", $dias))
                                    {!! Form::checkbox('dias[]','Domingo', true) !!}
                                @else
                                    {!! Form::checkbox('dias[]','Domingo')  !!}
                                @endif
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                            {!! Form::label('horarioInicio', 'Horário Inicial') !!}
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-clock-o fa-fw"></i></span>
                                {!! Form::text('horarioInicio', null, ['class'=>'form-control', 'placeholder'=>'20h30', 'data-mask'=>'99h99']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                            {!! Form::label('horarioFim', 'Horário Final') !!}
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-clock-o fa-fw"></i></span>
                                {!! Form::text('horarioFim', null, ['class'=>'form-control', 'placeholder'=>'20h30', 'data-mask'=>'99h99']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                            {!! Form::label('credioAula', 'Crédito por aula') !!}
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-usd fa-fw"></i></span>
                                {!! Form::text('credioAula', null, ['class'=>'form-control', 'placeholder'=>'1.5 ou 1.0', 'data-mask'=>'9.9']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    {!! Form::submit('Atualizar dados', ['class'=>'btn btn-primary pull-right']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection