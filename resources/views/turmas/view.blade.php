@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Turma de {{ $turma->modalidades->modalidade }}</h1>
                <p class="lead"><i class="fa fa-clock-o"></i> {{ $turma->horarioInicio }} às {{ $turma->horarioFim }}</p>
                <p class="lead"><i class="fa fa-user"></i> Professor: {{ $turma->professores->nome_prof }}</p>
                <p class="lead  "><i class="fa fa-calendar"></i>
                    <?php $dias = explode(",", $turma->diasSemana); ?>
                    @foreach($dias as $dia)
                    <span class="label label-info">{{ $dia }}</span>
                    @endforeach
                </p>
                <hr/>
                <p class="lead"></p>
                <p class="lead"></p>
                <p class="lead"></p>
                <p class="lead"></p>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><h2>Status dos últimos 7 dias</h2></div>
                    <div class="panel-body">
                        <!-- List group -->
                        <ul class="list-group">
                            <li class="list-group-item lead"><i class="fa fa-line-chart"></i> <strong>Frequeência média da semana:</strong> 25 alunos por dia de aula</li>
                            <li class="list-group-item lead"><i class="fa fa-credit-card"></i> <strong>Créditos arrecadados na semana:</strong> 25</li>
                            <li class="list-group-item lead"><i class="fa fa-usd"></i> <strong>Média de arrecadação:</strong> R$250,00</li>
                            <li class="list-group-item lead"><i class="fa fa-usd"></i> <strong>Custo Professor:</strong> R$75,00</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop