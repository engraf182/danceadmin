@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="jumbotron">
                    @is('alunos')
                        <img class="img-circle center-block" width="60" src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}">
                        {{-- <img class="ui medium circular image" src="/images/wireframe/square-image.png"> --}}
                        <h2 class="text-center">Olá {{ Auth::user()->name }}</h2>
                    @endis
                    <p class="text-center">Aguarde, na segunda feira teremos novidades aqui!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="ui statistics text-center" style="font-size: 50px;">
                            <div class="statistic">
                                <div class="value">
                                    <i class="fa fa-cubes icon"></i> {{ $aluno[0]->creditos['credito'] }}
                                </div>
                                <div class="label" style="color: #21ba45">
                                    Créditos
                                </div>
                            </div>
                        </div>{{-- /.ui statistics --}}
                    </div>
                </div>{{-- /.panel --}}
            </div>{{-- /.col-xs-12 col-md-4 --}}

        </div>
    </div>

@stop