@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="jumbotron">
                    <img class="img-circle center-block" width="60" src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}">
                    {{-- <img class="ui medium circular image" src="/images/wireframe/square-image.png"> --}}
                    <h2 class="text-center">Olá {{ Auth::user()->name }}</h2>
                    <p class="text-center">Texto</p>
                </div>
            </div>
        </div>
    </div>

@stop