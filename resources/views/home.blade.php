@extends('app')
@section('title', 'Home Apresentação')
@section('content')
<div class="container">
	<div class="row">
	    <div class="col-xs-12">
	        <div class="jumbotron">
	            <h1>Olá {{ Auth::user()->name }}</h1>
	            @is('editor')
	                
	            @endis
	            <p>Estamos quase terminando esse sistema</p>
	        </div>
	    </div>
	</div>
</div>
@endsection

