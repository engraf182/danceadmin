@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Albuns</h1>
                <a href="{{ route('album.create') }}" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-plus lg"></i></span>Novo album</a><br/><br/>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nome do Album</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($albuns as $album)
                                <tr>
                                    <td>{{ $album->title }}</td>
                                    <td>
                                        <a class="btn btn-danger" href="{{ route('album.destroy',['id'=>$album->id]) }}"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $albuns->render() !!}
            </div>
        </div>

@stop
