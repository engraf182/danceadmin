<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            {!! Form::label('title', 'Título:') !!}
            {!! Form::text('title', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            {!! Form::label('code', 'Código:') !!}
            {!! Form::textarea('code', null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>
<hr/>
<div class="form-group">
    {!! Form::submit($text_button, ['class'=>'btn btn-primary pull-right']) !!}
</div>
