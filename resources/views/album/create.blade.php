@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {{-- Bradcrumb --}}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Adicionar album</h1>
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::open(['route'=>'album.store']) !!}
                    @include('album.form', ['text_button' => 'Adicionar album'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
