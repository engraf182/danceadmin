
<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="content-language" content="pt-br">
	<title>Pr&eacute; Eventos</title>
	<meta name="author" content="Futurecom Development Team">
	<link rel="alternate" hreflang="es" href="http://es.futurecom.com.br/pre-eventos" />
	<link rel="alternate" hreflang="en" href="http://en.futurecom.com.br/pre-events" />
	<meta name="reply-to" content="design@futurecom.com.br">
	<meta name="robots" content="index,follow">
	<meta name="keywords" content="Pr&eacute; eventos no futurecom"> 
	<meta name="description" content="">

	<meta property="og:url" content="http://pt.futurecom.com.br/pre-eventos">
	<meta property="og:title" content="Pr&eacute; Eventos">
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Futurecom">
	<meta property="article:tag" content="Pr&eacute; eventos no futurecom">
	<meta property="og:description" content="">
	<meta property="og:image" content="http://pt.futurecom.com.br/images/img_site.jpg">
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="472">	<!-- Common head includes -->
	<!-- Favicons -->
	<!--[if IE]><link rel="shortcut icon" href="http://pt.futurecom.com.br/assets/images/favicon/favicon_16px.png"><![endif]-->
	<!--ICONS-->
	<link rel="icon" href="http://pt.futurecom.com.br/assets/images/favicon/favicon_16px.png" sizes="16x16">
	<link rel="icon" href="http://pt.futurecom.com.br/assets/images/favicon/favicon_32px.png" sizes="32x32">
	<link rel="icon" href="http://pt.futurecom.com.br/assets/images/favicon/favicon_64px.png" sizes="64x64">
	<link rel="icon" href="http://pt.futurecom.com.br/assets/images/favicon/favicon_128px.png" sizes="128x128">
	<meta name="msapplication-TileImage" content="http://pt.futurecom.com.br/assets/images/favicon/tile.png"/>
	<meta name="msapplication-TileColor" content="#00b3ea"/>

	<!-- Bootstrap core CSS -->
	<link href="http://pt.futurecom.com.br/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Bootstrap theme -->
	<link href="http://pt.futurecom.com.br/assets/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="http://pt.futurecom.com.br/assets/bootstrap/css/custom.css" rel="stylesheet">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- Bootstrap Jasny-Bootstrap core CSS -->
	<link href="http://pt.futurecom.com.br/assets/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="http://pt.futurecom.com.br/assets/jasny-bootstrap/custom.css" rel="stylesheet">
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<!-- Toggle Switch -->
	<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/css/toggle-switch.css">
	<!-- Bootstrap Datepicker -->
	<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/datepicker/css/datepicker.css">

	<!-- Icomoon FutureGlyphs -->
	<!-- <link rel="stylesheet" href="assets/futureglyphs/style.css"> -->
	<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/futureglyphs/style.css">
	<!-- Icomoon IE7 -->
	<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/futureglyphs/ie7/ie7.css">
	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href="http://pt.futurecom.com.br/assets/fonts/styles.css" rel="stylesheet">
	<!-- Extra Head Includes -->

<!-- FlexSlider -->
<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/flexslider/flexslider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/flexslider/custom.css">

<!-- animate.css -->
<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/css/animate.css">

<!-- Fancybox -->
<!-- Add fancyBox -->
<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/fancybox/custom.css" type="text/css" media="screen" />
<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/fancybox/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen" />
<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/fancybox/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
<!-- Site-Level CSS -->
<!-- Page-Level CSS -->
<link rel="stylesheet" href="http://pt.futurecom.com.br/assets/css/main.css">
<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="assets/css/home.css">
	<style>
		.bootbox h4.modal-title{
			text-align: center;
		}
		.bootbox-body{
			text-align: center;
		}
	</style>
	<style type="text/css">
		.dancing{
			color: #000;
		}
		h1{
			font-size: 10px;
		}
		.panel{
			font-size: 10px;
		}
	</style>
</head>
<body id="top" role="document">

		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-wide" role="navigation" style="margin-top: 35px;">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
						<span class="sr-only">Abrir menu</span> <i class="icon-menu"></i>
					</button>
					<a class="navbar-brand" href="http://pt.futurecom.com.br">
						<i id="icon-futurecom" class="icon-futurecom hidden-xs"></i>
						<img class="visible-xs" src="http://pt.futurecom.com.br/images/futurecom-sem-simbolo.png" alt="Futurecom">
					</a>
				</div>

				<div class="collapse navbar-collapse" id="navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="dropdown">
			<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				O Evento <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
																																																									<li>
							<a target="_self" href='sobre-o-futurecom' target='_self'>
								Sobre o Futurecom
															</a>
						</li>
																																			<li>
							<a target="_self" href='programacao' target='_self'>
								Hor&aacute;rios e Programa&ccedil;&atilde;o
															</a>
						</li>
																																																																							<li>
							<a target="_self" href='ingressos' target='_self'>
								Ingressos
																	<span class="label label-primary">Novo</span>
															</a>
						</li>
																																												<li>
							<a target="" href='apoio-institucional' target='_self'>
								Apoio Institucional
															</a>
						</li>
																											<li class="divider"></li>
												<li>
							<a target="_self" href='fornecedores' target='_self'>
								Fornecedores Oficiais
																	<span class="label label-warning">Atualizado</span>
															</a>
						</li>
																																												<li>
							<a target="_self" href='passagens-hospedagem' target='_self'>
								Passagens e Hospedagem
																	<span class="label label-warning">Atualizado</span>
															</a>
						</li>
																																				<li class="divider"></li>
												<li>
							<a target="_blank" href='http://www.futureshare.com.br/Futurecom_2015/Comunicacao_para_clientes/Estatisticas/Estatisticas-2014.pdf' target='_self'>
								Estat&iacute;sticas 2014
															</a>
						</li>
																																			<li>
							<a target="_self" href='depoimentos' target='_self'>
								Depoimentos 2014
															</a>
						</li>
																										<li>
							<a target="" href='fotos-2014' target='_self'>
								Fotos 2014
															</a>
						</li>
												</ul>
		</li>
		
			<li class="dropdown">
			<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				O Congresso <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
																					<li>
							<a target="_self" href='pre-eventos' target='_self'>
								Pr&eacute; Eventos
															</a>
						</li>
																																																																																									<li>
							<a target="_self" href='paineis' target='_self'>
								Pain&eacute;is
																	<span class="label label-warning">Atualizado</span>
															</a>
						</li>
																										<li>
							<a target="" href='coordenadores-de-paineis' target='_self'>
								Coordenadores de Pain&eacute;is
															</a>
						</li>
																																																					<li>
							<a target="_self" href='personalidades-confirmadas' target='_self'>
								Personalidades Confirmadas
																	<span class="label label-warning">Atualizado</span>
															</a>
						</li>
																																																						<li class="divider"></li>
												<li>
							<a target="_blank" href='http://tv.futurecom.com.br' target='_self'>
								Videos do Congresso 2014 (FuturecomTV)
															</a>
						</li>
																																																																											</ul>
		</li>
		
			<li class="dropdown">
			<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				A Exposi&ccedil;&atilde;o <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
																																							<li>
							<a target="_blank" href='http://www.futurecom.com.br/seja-um-expositor/pt/' target='_self'>
								Seja um Expositor
															</a>
						</li>
																																												<li>
							<a target="_self" href='patrocinadores' target='_self'>
								Patrocinadores
																	<span class="label label-warning">Atualizado</span>
															</a>
						</li>
																																																																							<li>
							<a target="_self" href='http://www.futureshare.com.br/Futurecom_2015/Layout/Layout_2015.pdf' target='_self'>
								Layout 2015
																	<span class="label label-warning">Atualizado</span>
															</a>
						</li>
																																												<li>
							<a target="_blanck" href='http://www.futurecom.com.br/merchandising/pt' target='_self'>
								Portfolio de Merchandising
															</a>
						</li>
																																																						<li class="divider"></li>
												<li>
							<a target="_self" href='http://www.futureshare.com.br/Futurecom_2015/Operacional/Manual_Expositor/Tabela_Taxas/Tabela_Taxas_Futurecom_2015.pdf' target='_self'>
								Tabela de Taxas
																	<span class="label label-primary">Novo</span>
															</a>
						</li>
																																				<li class="divider"></li>
												<li>
							<a target="_blank" href='http://www.provisuale.com.br/Infra2015T/frm01login.aspx?lg=pt' target='_self'>
								Expositor Online
																	<span class="label label-primary">Novo</span>
															</a>
						</li>
																																			<li>
							<a target="_blank" href='http://www.futureshare.com.br/futurecom_2015/operacional/manual_expositor/manual_do_expositor_2015.pdf' target='_self'>
								Manual do Expositor 2015
															</a>
						</li>
																														</ul>
		</li>
		
			<li class="dropdown">
			<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				Imprensa <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
																																																<li>
							<a target="_self" href='media-partners' target='_self'>
								Media Partners
																	<span class="label label-warning">Atualizado</span>
															</a>
						</li>
																																																					<li>
							<a target="" href='brand-kit' target='_self'>
								Brand Kit
															</a>
						</li>
																																													<li class="divider"></li>
												<li>
							<a target="_self" href='http://www.futurecom.com.br/credenciamento-imprensa/' target='_self'>
								Credenciamento de Imprensa
																	<span class="label label-primary">Novo</span>
															</a>
						</li>
																																																																																																																																										</ul>
		</li>
		
			<li class="dropdown">
			<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				Inova&ccedil;&otilde;es <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
																														<li>
							<a target="_self" href='http://www.futurecom.com.br/startup-session/pt/' target='_self'>
								StartUp Session
																	<span class="label label-primary">Novo</span>
															</a>
						</li>
																																																																																																																																																																																																																											</ul>
		</li>
		
			<li>
			<a target="" href="contato" target="_self">
				Contato
			</a>
		</li>
		
			<li>
			<a target="_blanck" href="http://www.futurecom.com.br/blog/" target="_self">
				Blog
			</a>
		</li>
		
			<li>
			<a target="_blanck" href="http://allyear.futurecom.com.br/" target="_self">
				All Year
			</a>
		</li>
		
			<li>
			<a target="_blanck" href="http://tv.futurecom.com.br" target="_self">
				TV
			</a>
		</li>
		
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li role="group" aria-label="Linguagem">
							<button id="btn-en" onclick="window.location.href='http://en.futurecom.com.br/pre-events'" type="button" class="btn btn-circle btn-outline btn-white navbar-btn"><i class="icon-en"></i></button>
							<button id="btn-es" onclick="window.location.href='http://es.futurecom.com.br/pre-eventos'" type="button" class="btn btn-circle btn-outline btn-white navbar-btn"><i class="icon-es"></i></button>
							<button id="btn-pt" onclick="window.location.href='http://pt.futurecom.com.br/pre-eventos'" type="button" class="btn btn-circle btn-outline btn-white navbar-btn"><i class="icon-pt"></i></button>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div>
		</nav>
		<div id="header-image" style="margin-top: -20px;">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div id="header-img" class="text-center">
							<img src="http://pt.futurecom.com.br/images/futurecom-2015.png" class="img-responsive img-center" alt="Futurecom 2015">
						</div>
						<div id="header-text">
							<p class="text-center museo">26 a 29 de outubro &nbsp; Transamérica Expo . São Paulo</p>
						</div>
						<div id="header-nav">
							<div class="text-center" id="header-img-btn">
								<div class="hidden-xs">
									<a class="btn btn-outline btn-white btn-lg" href="http://pt.futurecom.com.br/ingressos">
										<i class="icon-ingresso"></i> Ingressos
									</a>
									<a class="btn btn-outline btn-white btn-lg" target="_blank" href="http://www.futurecom.com.br/seja-um-expositor/pt/">
										<i class="icon-estande"></i>Seja um Expositor!
									</a>
									<a class="btn btn-outline btn-white btn-lg" target="_self" href="http://www.futureshare.com.br/Futurecom_2015/Layout/Layout_2015.pdf">
										<i class="icon-mapa"></i> Layout
									</a>
									<a class="btn btn-outline btn-white btn-lg" target="_self" href="http://pt.futurecom.com.br/patrocinadores">
										<i class="icon-flamula-estrela"></i> Patrocinadores
									</a>
									<a class="btn btn-outline btn-white btn-lg" target="_self" href="http://pt.futurecom.com.br/paineis">
										<i class="icon-flipchart"></i> Painéis
									</a>
								</div>								<div class="visible-xs">
									<a data-toggle="collapse" href="#menu-mobile" aria-expanded="false" aria-controls="menu-mobile" class="btn btn-default btn-lg btn-block collapsed">
										<i class="icon-menu"></i>
										<span style="display: none;" class="lang-en">Shortcuts</span>
										<span style="display: none;" class="lang-es">Shortcuts</span>
										<span style="display: inline;" class="lang-pt">Atalhos</span>
									</a>
									<div style="height: 0px;" aria-expanded="false" id="menu-mobile" class="collapse">
										<a class="btn btn-outline btn-white btn-lg" href="http://pt.futurecom.com.br/ingressos">
											<i class="icon-ingresso"></i> Ingressos
										</a>
										<a class="btn btn-outline btn-white btn-lg btn-block" target="_blank" href="http://www.futurecom.com.br/seja-um-expositor/pt/">
											<i class="icon-estande"></i>Seja um Expositor!
										</a>
										<a class="btn btn-outline btn-white btn-lg btn-block" target="_self" href="http://www.futureshare.com.br/Futurecom_2015/Layout/Layout_2015.pdf">
											<i class="icon-mapa"></i> Floorplan
										</a>
										<a class="btn btn-outline btn-white btn-lg btn-block" target="_self" href="http://pt.futurecom.com.br/patrocinadores.php">
											<i class="icon-flamula-estrela"></i> Patrocinadores
										</a>
										<a class="btn btn-outline btn-white btn-lg" target="_self" href="http://pt.futurecom.com.br/paineis">
											<i class="icon-flipchart"></i> Painéis
										</a>
									</div>
								</div>							</div>						</div>					</div>				</div>			</div>		</div>		<main>
			<div class="wrapper">
				<div class="container-fluid">
					<section id="future-form" class="row">
						<div class="col-xs-12">
							@yield('content')
						</div>
					</section>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row">
					<footer>
						<div class="container" style="padding-bottom: 20px;">
							<div class="row">
								<div id="quick-links" class="col-xs-12 col-sm-3 col-md-4">
									<div class="row">
										<div class="col-xs-12">
											<h3>Quick Links</h3>
											<ul>
												<a href="http://blog.futurecom.com.br">
													<li>Futurecom Blog</li>
												</a>
												<a href="http://floorplan.futurecom.com.br">
													<li>Futurecom 2015 Floorplan</li>
												</a>
												<a href="http://tickets.futurecom.com.br">
													<li>Compre o Seu Ingresso</li>
												</a>
												<a href="http://pt.futurecom.com.br/contato">
													<li>Contato</li>
												</a>
											</ul>
										</div><!-- ./col-xs-12 -->
										<div id="social" class="col-xs-12">
											<br />
											Futurecom nas Mídias Sociais:
											<ul>
												<li class="pull-left" style="margin: 0 5px;">
													<a href="https://www.facebook.com/futurecom" class="btn btn-circle btn-facebook" target="_blank"><i class="icon-facebook"></i></a>
												</li>
												<li class="pull-left" style="margin: 0 5px;">
													<a href="https://twitter.com/FuturecomEvent" class="btn btn-circle btn-twitter" target="_blank"><i class="icon-twitter"></i></a>
												</li>
												<li class="pull-left" style="margin: 0 5px;">
													<a href="https://www.youtube.com/user/futurecomevent/?sub_confirmation=1" class="btn btn-circle btn-youtube" target="_blank"><i class="icon-youtube"></i></a>
												</li>
												<div class="clearfix visible-md"></div>
												<li class="pull-left" style="margin: 0 5px;">
													<a href="http://www.linkedin.com/groups?home=&gid=1365937&trk=anet_ug_hm" class="btn btn-circle btn-linkedin" target="_blank"><i class="icon-linkedin"></i></a>
												</li>
												<div class="clearfix visible-lg"></div>
												<li class="pull-left" style="margin: 0 5px;">
													<a href="https://plus.google.com/117063480866807446503/videos" class="btn btn-circle btn-google-plus" target="_blank"><i class="icon-google-plus"></i></a>
												</li>
												<li class="pull-left" style="margin: 0 5px;">
													<a href="http://www.flickr.com/FuturecomEvent" class="btn btn-circle btn-flickr" target="_blank"><i class="icon-flickr"></i></a>
												</li>
												<li class="pull-left" style="margin: 0 5px;">
													<a href="http://instagram.com/futurecomevent" class="btn btn-circle btn-instagram" target="_blank"><i class="icon-instagram"></i></a>
												</li>
											</ul>
											<br /><br /><hr />
											Futurecom <strong>1.2.28
</strong> desenvolvido por <span class="visible-xs">Provisuale | Informa.</span>
										</div><!-- ./col-xs-12 -->
										<div classs="col-xs-12">
											<img src="http://pt.futurecom.com.br/images/provisuale_informa.png" height="37" width="202" alt="Provisuale | Informa" class="hidden-xs">
										</div>
									</div>
								</div><!-- ./col-xs-12 -->
								<div class="col-sm-9 col-md-8 hidden-xs">
									<div class="row">
										<div class="col-xs-4">
										<h3>O Evento</h3>
				<ul>
																																																														<a target="_self" href='sobre-o-futurecom'>
								<li>
									Sobre o Futurecom
																	</li>
							</a>
																																			<a target="_self" href='programacao'>
								<li>
									Hor&aacute;rios e Programa&ccedil;&atilde;o
																	</li>
							</a>
																																																																															<a target="_self" href='ingressos'>
								<li>
									Ingressos
																			<span class="label label-primary">Novo</span>
																	</li>
							</a>
																																														<a target="" href='apoio-institucional'>
								<li>
									Apoio Institucional
																	</li>
							</a>
																								<a target="_self" href='fornecedores'>
								<li>
									Fornecedores Oficiais
																			<span class="label label-warning">Atualizado</span>
																	</li>
							</a>
																																														<a target="_self" href='passagens-hospedagem'>
								<li>
									Passagens e Hospedagem
																			<span class="label label-warning">Atualizado</span>
																	</li>
							</a>
																																			<a target="_blank" href='http://www.futureshare.com.br/Futurecom_2015/Comunicacao_para_clientes/Estatisticas/Estatisticas-2014.pdf'>
								<li>
									Estat&iacute;sticas 2014
																	</li>
							</a>
																																			<a target="_self" href='depoimentos'>
								<li>
									Depoimentos 2014
																	</li>
							</a>
																								<a target="" href='fotos-2014'>
								<li>
									Fotos 2014
																	</li>
							</a>
															</ul>
													</div>
					<div class="col-xs-4">
															
										<h3>O Congresso</h3>
				<ul>
																		<a target="_self" href='pre-eventos'>
								<li>
									Pr&eacute; Eventos
																	</li>
							</a>
																																																																																																					<a target="_self" href='paineis'>
								<li>
									Pain&eacute;is
																			<span class="label label-warning">Atualizado</span>
																	</li>
							</a>
																								<a target="" href='coordenadores-de-paineis'>
								<li>
									Coordenadores de Pain&eacute;is
																	</li>
							</a>
																																																									<a target="_self" href='personalidades-confirmadas'>
								<li>
									Personalidades Confirmadas
																			<span class="label label-warning">Atualizado</span>
																	</li>
							</a>
																																																									<a target="_blank" href='http://tv.futurecom.com.br'>
								<li>
									Videos do Congresso 2014 (FuturecomTV)
																	</li>
							</a>
																																																																																												</ul>
													</div>
					<div class="col-xs-4">
															
										<h3>A Exposi&ccedil;&atilde;o</h3>
				<ul>
																																								<a target="_blank" href='http://www.futurecom.com.br/seja-um-expositor/pt/'>
								<li>
									Seja um Expositor
																	</li>
							</a>
																																														<a target="_self" href='patrocinadores'>
								<li>
									Patrocinadores
																			<span class="label label-warning">Atualizado</span>
																	</li>
							</a>
																																																																															<a target="_self" href='http://www.futureshare.com.br/Futurecom_2015/Layout/Layout_2015.pdf'>
								<li>
									Layout 2015
																			<span class="label label-warning">Atualizado</span>
																	</li>
							</a>
																																														<a target="_blanck" href='http://www.futurecom.com.br/merchandising/pt'>
								<li>
									Portfolio de Merchandising
																	</li>
							</a>
																																																									<a target="_self" href='http://www.futureshare.com.br/Futurecom_2015/Operacional/Manual_Expositor/Tabela_Taxas/Tabela_Taxas_Futurecom_2015.pdf'>
								<li>
									Tabela de Taxas
																			<span class="label label-primary">Novo</span>
																	</li>
							</a>
																																			<a target="_blank" href='http://www.provisuale.com.br/Infra2015T/frm01login.aspx?lg=pt'>
								<li>
									Expositor Online
																			<span class="label label-primary">Novo</span>
																	</li>
							</a>
																																			<a target="_blank" href='http://www.futureshare.com.br/futurecom_2015/operacional/manual_expositor/manual_do_expositor_2015.pdf'>
								<li>
									Manual do Expositor 2015
																	</li>
							</a>
																																					</ul>
													</div>
					<div class="col-xs-4">
															
										<h3>Imprensa</h3>
				<ul>
																																																			<a target="_self" href='media-partners'>
								<li>
									Media Partners
																			<span class="label label-warning">Atualizado</span>
																	</li>
							</a>
																																																									<a target="" href='brand-kit'>
								<li>
									Brand Kit
																	</li>
							</a>
																																														<a target="_self" href='http://www.futurecom.com.br/credenciamento-imprensa/'>
								<li>
									Credenciamento de Imprensa
																			<span class="label label-primary">Novo</span>
																	</li>
							</a>
																																																																																																																																																																									</ul>
													</div>
					<div class="col-xs-4">
															
										<h3>Inova&ccedil;&otilde;es</h3>
				<ul>
																													<a target="_self" href='http://www.futurecom.com.br/startup-session/pt/'>
								<li>
									StartUp Session
																			<span class="label label-primary">Novo</span>
																	</li>
							</a>
																																																																																																																																																																																																																																																																												</ul>
													</div>
					<div class="col-xs-4">
															
										<h3>
					<a target="" href="contato" target="_self">
						Contato
					</a>
				</h3>
										
										<h3>
					<a target="_blanck" href="http://www.futurecom.com.br/blog/" target="_self">
						Blog
					</a>
				</h3>
										
										<h3>
					<a target="_blanck" href="http://allyear.futurecom.com.br/" target="_self">
						All Year
					</a>
				</h3>
										
										<h3>
					<a target="_blanck" href="http://tv.futurecom.com.br" target="_self">
						TV
					</a>
				</h3>
										
	</div>									</div><!-- ./row -->
								</div><!-- ./col-md-8 -->
							</div><!-- ./row -->
						</div><!-- ./container -->

						<div class="informa-wrapp">
							<div class="footer">
							 	<div id="globalFooter" class="clearfix">
									<div id="globalFooterContent">
									<div  class="container">
									  <div id="Table_01" class="col-md-12">
											<div id="logoContainer">
												<a href="http://www.informaexhibitions.com/" title="This link will open in the new browser window"> <img alt="Informa Exhibitions" class="informa" src="http://www.informaexhibitions.com/images/informa_exhibitions-new-logo.png"></a>
											 </div>
										  <div class="text-copyright">
							              	<p>Copyright &copy; 2015. All rights reserved. Informa Exhibitions.</p>
										  <a href="http://www.btsinforma.com.br/pt/privacy-policy" class="pull-right" title="This link will open in the new browser window">Privacy and Cookie Policy</a></div>
										</div>
									  </div>
									</div>
							 	</div>
							</div> <!-- footer end -->
						</div><!-- /.informa-wrapp -->

					</footer>
				</div><!-- ./row -->
			</div><!-- ./container-fluid -->

		</main>

	<!-- Bootstrap core JavaScript -->
	<script src="http://pt.futurecom.com.br/assets/bootstrap/js/bootstrap.min.js"></script>

	<!-- Jasny Bootstrap core JavaScript -->
	<script src="http://pt.futurecom.com.br/assets/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>


	<!-- Fancybox -->
	<!-- Add fancyBox -->
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/fancybox/jquery.fancybox.pack.js"></script>
	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/fancybox/helpers/jquery.fancybox-buttons.js"></script>
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/fancybox/helpers/jquery.fancybox-media.js"></script>
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/fancybox/helpers/jquery.fancybox-thumbs.js"></script>


	<!-- FlexSlider -->
	<script src="http://pt.futurecom.com.br/assets/flexslider/jquery.flexslider.js"></script>


	<!-- FlexSlider -->
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/flexslider/demo/js/shCore.js"></script>
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/flexslider/demo/js/shBrushXml.js"></script>
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/flexslider/demo/js/shBrushJScript.js"></script>
	<!-- Optional FlexSlider Additions -->
	<script src="http://pt.futurecom.com.br/assets/flexslider/demo/js/jquery.easing.js"></script>
	<script src="http://pt.futurecom.com.br/assets/flexslider/demo/js/jquery.mousewheel.js"></script>
	<!-- ./ FlexSlider -->

	<!-- Quicksand.js -->
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/js/jquery.quicksand.js"></script>

	<!-- BootBox.js -->
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/js/bootbox.min.js"></script>

	<!-- CUSTOM SCRIPTS -->
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/js/custom.js"></script>


	<!-- Extra Scripts Includes -->

	<!-- WOW.js -->
	<script src="http://pt.futurecom.com.br/assets/js/wow.min.js"></script>

	<!-- HIDESEEK -->
	<script src="http://pt.futurecom.com.br/assets/js/jquery.hideseek.min.js"></script>

	<!-- TAGCLOUD -->
	<script src="http://pt.futurecom.com.br/assets/js/jquery.tagcanvas.min.js"></script>


	<!-- CUSTOM SCRIPTS -->
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/js/custom-extras.js"></script>
	<script type="text/javascript" src="http://pt.futurecom.com.br/assets/informa/ribbon.js"></script>
</body>
</html>
