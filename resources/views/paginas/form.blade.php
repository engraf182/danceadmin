<div class="row">
    <div class="col-xs-12 col-md-12">
    <div class="form-group">
        {!! Form::label('titulo_pag', 'Título da página:') !!}
        {!! Form::text('titulo_pag', null, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('content_pagina', 'Conteúdo da página:') !!}
        {!! Form::textarea('content_pagina', null, ['class' => 'ckeditor', 'id' => 'editor']) !!}
    </div>
    </div>
</div>

<hr/>
<div class="form-group">
    {!! Form::submit($text_button, ['class'=>'btn btn-primary pull-right']) !!}
</div>
