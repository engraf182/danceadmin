@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Páginas</h1>
                {{-- <a href="{{ route('paginas.create') }}" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-plus lg"></i></span>Adicionar nova página</a><br/><br/> --}}
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Título da página</th>
                                <th></th>
                            </tr>
                            <!--
                                $table->increments('id');
                                $table->string('titulo_pag');
                                $table->longText('content');
                                $table->timestamps();
                            -->
                        </thead>
                        <tbody>
                            @foreach($paginas as $pag)
                                <tr>
                                    <td>{{ $pag->titulo_pag }}</td>
                                    <td class="crud-button">
                                        <a class="btn btn-success" href="{{ route('paginas.edit',['id'=>$pag->id]) }}"><i class="fa fa-pencil-square-o"></i></a>
                                        {{-- <a class="btn btn-danger" onclick="return confirm('Deseja deletar a página ?')" href="{{ route('paginas.destroy',['id'=>$pag->id]) }}"><i class="fa fa-trash-o"></i></a> --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

@stop