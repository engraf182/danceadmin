@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Adicionar Página</h1>
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::open(['route'=>['paginas.store']]) !!}
                @include('paginas.form', ['text_button' => 'Salvar'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection