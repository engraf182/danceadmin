@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Editar página</h1>
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::model($pagina, ['route'=>['paginas.update', $pagina->id], 'method'=>'put']) !!}
                @include('paginas.form', ['text_button' => 'Atualizar Dados'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection