<div class="modal fade" id="print_{{ $aluno->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Configuração da etiqueta</h4>
            </div>
            {!! Form::open(['route'=>['alunos.carteirinha']]) !!}
                <input type="hidden" name="codigo" value="{{ $aluno->id }}">
                <input type="hidden" name="nome" value="{{ $aluno->nome }}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <p>Selecione a posição da etiqueta</p>
                            <div class="btn-group" data-toggle="buttons">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="btn btn-warning btn-xs active" style="width: 25px;">
                                            <input type="radio" name="classe" value="a1" autocomplete="off" checked style="display: none;"> A1
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="a2" autocomplete="off" style="display: none;"> A2
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="a3" autocomplete="off" style="display: none;"> A3
                                        </label>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 4px;">
                                    <div class="col-xs-12">
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="b1" autocomplete="off" style="display: none;"> B1
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="b2" autocomplete="off" style="display: none;"> B2
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="b3" autocomplete="off" style="display: none;"> B3
                                        </label>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 4px;">
                                    <div class="col-xs-12">
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="c1" autocomplete="off" style="display: none;"> C1
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="c2" autocomplete="off" style="display: none;"> C2
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="c3" autocomplete="off" style="display: none;"> C3
                                        </label>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 4px;">
                                    <div class="col-xs-12">
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="d1" autocomplete="off" style="display: none;"> D1
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="d2" autocomplete="off" style="display: none;"> D2
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="d3" autocomplete="off" style="display: none;"> D3
                                        </label>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 4px;">
                                    <div class="col-xs-12">
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="e1" autocomplete="off" style="display: none;"> E1
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="e2" autocomplete="off" style="display: none;"> E2
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="e3" autocomplete="off" style="display: none;"> E3
                                        </label>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 4px;">
                                    <div class="col-xs-12">
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="f1" autocomplete="off" style="display: none;"> F1
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="f2" autocomplete="off" style="display: none;"> F2
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="f3" autocomplete="off" style="display: none;"> F3
                                        </label>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 4px;">
                                    <div class="col-xs-12">
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="g1" autocomplete="off" style="display: none;"> G1
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="g2" autocomplete="off" style="display: none;"> G2
                                        </label>
                                        <label class="btn btn-warning btn-xs" style="width: 25px;">
                                            <input type="radio" name="classe" value="g3" autocomplete="off" style="display: none;"> G3
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <img class="center-block" src="{{ asset('images/pimaco.png') }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Imprimir</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>