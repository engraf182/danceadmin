<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Dance Sempre – Espaço Cultural">
	<meta name="author" content="Gregory Engraf &amp; Gustavo Tonietto">
	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<!-- FlexSlider -->
	<link rel="stylesheet" href="assets/flexslider/flexslider.css" type="text/css" media="screen" />
</head>

<body role="document">
	<main>
		<!-- Wrapper -->
		<div class="wrapper">
			@yield('content')
		</div><!-- /.Wrapper -->
	</main>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	
	<!-- FlexSlider -->
	<script type="text/javascript" src="{{ asset('assets/flexslider/flexslider-min.js') }}"></script>
	<script type="text/javascript">
	  $(function(){
	    SyntaxHighlighter.all();
	  });
	  $(window).load(function(){
	    $('.flexslider').flexslider({
	      animation: "slide",
	      start: function(slider){
	        $('body').removeClass('loading');
	      }
	    });
	  });
	</script>
	<!-- Optional FlexSlider Additions -->
	<script src="{{ asset('assets/flexslider/demo/js/jquery.easing.js') }}"></script>
	<script src="{{ asset('assets/flexslider/demo/js/jquery.mousewheel.js') }}"></script>
</body>
</html>