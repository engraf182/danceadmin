@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {{-- Bradcrumb --}}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-lg-6">
                <p style="margin-top: 20px;">
                    <button type="button" data-toggle="modal" data-target="#busca_aluno" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-search lg"></i></span>Buscar Aluno</button>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Adicionar Aluno</h1>
                    {{-- <h1>Adicionar Painelista</h1> --}}
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::open(['route'=>['alunos.store']]) !!}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-2 col-md-2">
                                <fieldset disabled="">
                                    <label>Data de Mátricula</label>
                                    <input type="text" class="form-control disabled" name="hora_final" value="<?php echo date("d") . "/" . date("m") . "/" . date("Y")?>">
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-9">
                            <div class="form-group">
                                {!! Form::label('nome', 'Nome:') !!}
                                {!! Form::text('nome', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                {!! Form::label('nascimento', 'Data de Nascimento:') !!}
                                {!! Form::input('text', 'nascimento', null, ['class'=>'form-control', 'data-mask'=>'99/99/9999']) !!}
                            </div>
                        </div>
                    </div>
                    @include('alunos.form', ['text_button' => 'Adicionar Pessoa'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('assets/busca_aluno/ajax_alunos_sist_antigo.js') }}"></script>
    <div class="modal fade" id="busca_aluno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Buscar</h4>
                </div>
                
                    <input type="hidden" name="id_menu" value="1">
                    <div class="modal-body">
                        <div class="form-group input-group">
                            <input type="text" class="form-control" name="txtnome" id="txtnome" placeholder="Consultar sistema antigo">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" onclick="getDados();">
                                    <i class="fa fa-search lg"></i>
                                </button>
                            </span>
                        </div>
                        <div id="Resultado" style="max-height: 60vh; overflow-y:auto;"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection