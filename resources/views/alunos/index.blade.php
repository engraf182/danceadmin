@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {{-- Bradcrumb --}}
            </div>
        </div>
        {{--$table->increments('id');
            $table->string('nome', 150);
            $table->string('endereco', 255);
            $table->string('bairro', 55);
            $table->string('tel_celular', 55);
            $table->string('tel_fixo', 55);
            $table->string('email', 150);
            $table->string('obs', 150);
            $table->integer('matricula');
            $table->date('nascimento');
            $table->timestamps(); --}}
        <div class="row">
            <div class="col-xs-12">
                <h1>Alunos</h1>
                {{-- <h1>Painelistas - Futurecom</h1> --}}
                <a href="{{ route('alunos.create') }}" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-plus lg"></i></span>Adicionar</a><br/><br/>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Celular</th>
                                <th>Fixo</th>
                                <th>Crédito</th>
                                <th>Data Nasc</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($alunos as $aluno)
                                <tr>
                                    <td><strong>{{ $aluno->nome }}</strong></td>
                                    <td>{{ $aluno->email }}</td>
                                    <td>{{ $aluno->tel_celular }}</td>
                                    <td>{{ $aluno->tel_fixo }}</td>
                                    <td><span class="badge">{{ $aluno->creditos['credito'] }}</span></td>
                                    <td>{{ $aluno->nascimento->format('d/m/Y') }}</td>
                                    <td>
                                        <button type="button" data-toggle="modal" data-target="#print_{{ $aluno->id }}" class="btn btn-primary" title="Imprimir carteirinha"><i class="fa fa-print"></i></button>
                                        <a class="btn btn-warning" title="Adicionar Crédito" href="{{ route('alunos.insert_credit',['id'=>$aluno->id]) }}"><i class="fa fa-plus-circle"></i></a>
                                        <a class="btn btn-success" title="Editar Aluno" href="{{ route('alunos.edit',['id'=>$aluno->id]) }}"><i class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-danger" title="Excluir Aluno" href="{{ route('alunos.destroy',['id'=>$aluno->id]) }}"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                @include('includes.carteirinha')
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
@stop