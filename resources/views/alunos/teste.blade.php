@extends('app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			    <div class="page-header">
			        {{-- <h1>Adicionar Aluno</h1> --}}
			        <h1>Alunos Sistema Antigo</h1>
			    </div>
			</div>
			<div class="col-xs-12">
				<table class="table table-striped">
				    <thead>
				        <tr>
				            <th>Nome</th>
				            <th>Telefones</th>
				            <th>Email</th>
				            <th>Endereço</th>
				        </tr>
				    </thead>
				    <tbody>
				        @foreach($alunos as $aluno)
				            <tr>
				                <td>{{ $aluno->aluno_nome }}</td>
				                <td>{{ $aluno->aluno_telefone }} / {{ $aluno->aluno_celular }}</td>
				                <td>{{ $aluno->aluno_email }}</td>
				                <td><small>{{ $aluno->aluno_endereco }}, {{ $aluno->aluno_bairro }}</small></td>
				            </tr>
				        @endforeach
				    </tbody>
				</table>
			</div>
		</div>
	</div>
@stop