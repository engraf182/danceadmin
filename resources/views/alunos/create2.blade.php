@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {{-- Bradcrumb --}}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Adicionar Aluno</h1>
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::open(['route'=>['alunos.store']]) !!}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-2 col-md-2">
                                <fieldset disabled="">
                                    <label>Data de Mátricula</label>
                                    <input type="text" class="form-control disabled" name="hora_final" value="<?php echo date("d") . "/" . date("m") . "/" . date("Y")?>">
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-9">
                            <div class="form-group">
                                {!! Form::label('nome', 'Nome:') !!}
                                {!! Form::text('nome', $aluno->aluno_nome, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                <?php 
                                if (!empty($aluno->aluno_data_nasc)) {
                                    $nasc = explode('-', $aluno->aluno_data_nasc); 
                                    $nasc = $nasc[2]."/".$nasc[1]."/".$nasc[0];
                                }else{
                                    $nasc = null;
                                }
                                    
                                ?>
                                {!! Form::label('nascimento', 'Data de Nascimento:') !!}
                                {!! Form::input('text', 'nascimento', $nasc, ['class'=>'form-control', 'data-mask'=>'99/99/9999']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                {!! Form::label('tel_fixo', 'Tel Fixo:') !!}
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                                    {!! Form::text('tel_fixo', $aluno->aluno_telefone, ['class'=>'form-control', 'placeholder'=>'41 3333-3333', 'data-mask'=>'99 9999-9999']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                {!! Form::label('tel_celular', 'Tel Celular:') !!}
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-mobile fa-fw"></i></span>
                                    {!! Form::text('tel_celular', $aluno->aluno_celular, ['class'=>'form-control', 'placeholder'=>'41 8888-8888', 'data-mask'=>'99 9999-9999']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                {!! Form::label('email', 'Email:') !!}
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                                    {!! Form::input('email', 'email', $aluno->aluno_email, ['class'=>'form-control']) !!}
                                </div>
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-10">
                            <div class="form-group">
                                {!! Form::label('endereco', 'Endereço completo:') !!}
                                {!! Form::input('endereco', 'endereco', $aluno->aluno_endereco, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <div class="form-group">
                                {!! Form::label('bairro', 'Bairro:') !!}
                                {!! Form::input('bairro', 'bairro', $aluno->aluno_bairro, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                {!! Form::label('obs', 'Observações:') !!}
                                {!! Form::textarea('obs', $aluno->aluno_obs, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group">
                        {!! Form::submit("Adicionar aluno", ['class'=>'btn btn-primary pull-right']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection