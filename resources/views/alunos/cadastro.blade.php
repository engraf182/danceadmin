@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Cadastros feitos no site</h1>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Celular</th>
                                <th>Fixo</th>
                                <th>Data Cadastro</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($alunos as $aluno)
                                <tr>
                                    <td><strong>{{ $aluno->nome }}</strong></td>
                                    <td>{{ $aluno->email }}</td>
                                    <td>{{ $aluno->tel_celular }}</td>
                                    <td>{{ $aluno->tel_fixo }}</td>
                                    <td>{{ $aluno->created_at->format('d/m/Y') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@stop
