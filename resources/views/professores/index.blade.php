@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {{-- Breadcrumb --}}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    {{-- <h1>Adicionar Aluno</h1> --}}
                    <h1>Professores</h1>
                </div>
            </div>
            <div class="col-xs-12">
                <a href="{{ route('professores.create') }}" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-plus lg"></i></span>Adicionar Professor</a><br/><br/>
                <div class="table-responsive">
                    <table class="table table-striped" id="professores">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Celular</th>
                                <th>Fixo</th>
                                <th>Data Nasc</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($professores as $prof)
                                <tr>
                                    <td>{{ $prof->nome_prof }}</td>
                                    <td>{{ $prof->email }}</td>
                                    <td>{{ $prof->fone_movel }}</td>
                                    <td>{{ $prof->fone_fixo }}</td>
                                    <td>{{ $prof->data_nasc->format('d/m/Y') }}</td>
                                    <td>
                                        <a class="btn btn-warning" title="Veja os dados de: {{ $prof->nome_prof }}" href="{{ route('professores.view',['id'=>$prof->id]) }}"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-success" href="{{ route('professores.edit',['id'=>$prof->id]) }}"><i class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-danger" href="{{ route('professores.destroy',['id'=>$prof->id]) }}"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $professores->render() !!}
            </div>
        </div>

@stop