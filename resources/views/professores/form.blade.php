<div class="row">
    <div class="col-xs-12 col-md-3">
        <div class="form-group">
            {!! Form::label('fone_fixo', 'Tel Fixo:') !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                {!! Form::text('fone_fixo', null, ['class'=>'form-control', 'placeholder'=>'41 3333-3333', 'data-mask'=>'99 9999-9999']) !!}
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3">
        <div class="form-group">
            {!! Form::label('fone_movel', 'Tel Celular:') !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-mobile fa-fw"></i></span>
                {!! Form::text('fone_movel', null, ['class'=>'form-control', 'placeholder'=>'41 8888-8888', 'data-mask'=>'99 9999-9999']) !!}
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                {!! Form::input('email', 'email', null, ['class'=>'form-control']) !!}
            </div>
            <p class="help-block"></p>
        </div>
    </div>
</div>
<hr/>
<div class="form-group">
    {!! Form::submit($text_button, ['class'=>'btn btn-primary pull-right']) !!}
</div>