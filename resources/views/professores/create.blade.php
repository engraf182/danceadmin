@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {{-- Bradcrumb --}}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Adicionar Professor</h1>
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::open(['route'=>['professores.store']]) !!}
                    <div class="row">
                        <div class="col-xs-12 col-md-9">
                            <div class="form-group">
                                {!! Form::label('nome_prof', 'Nome:') !!}
                                {!! Form::text('nome_prof', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                {!! Form::label('data_nasc', 'Data de Nascimento:') !!}
                                {!! Form::input('text', 'data_nasc', null, ['class'=>'form-control', 'data-mask'=>'99/99/9999']) !!}
                            </div>
                        </div>
                    </div>
                    @include('professores.form', ['text_button' => 'Adicionar Pessoa'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection