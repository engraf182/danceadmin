@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Editar Professor</h1>
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::model($professor, ['route'=>['professores.update', $professor->id], 'method'=>'put']) !!}
                <div class="row">
                    <div class="col-xs-12 col-md-9">
                        <div class="form-group">
                            {!! Form::label('nome_prof', 'Nome:') !!}
                            {!! Form::text('nome_prof', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group">
                            {!! Form::label('data_nasc', 'Data de Nascimento:') !!}
                            {!! Form::input('text', 'data_nasc', $professor->data_nasc->format('d/m/Y'), ['class'=>'form-control', 'data-mask'=>'99/99/9999']) !!}
                        </div>
                    </div>
                </div>
                @include('professores.form', ['text_button' => 'Atualizar Dados'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection