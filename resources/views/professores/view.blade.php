@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>{{ $prof->nome_prof }}</h1>
                <p class="lead"><i class="fa fa-mobile"></i> {{ $prof->fone_fixo }}  <i class="fa fa-phone"></i> {{ $prof->fone_fixo }}</p>
                <p class="lead"><i class="fa fa-mail"></i> {{ $prof->email }}</p>
                <p class="lead"><i class="fa fa-calendar"></i> Aniversário: {{ $prof->data_nasc->format('d/m') }}</p>
                <hr/>
                <p class="lead"></p>
                <p class="lead"></p>
                <p class="lead"></p>
                <p class="lead"></p>
            </div>

            <div class="col-xs-12 col-md-6">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><strong>Histório de Pagamentos</strong></div>
                    <div class="panel-body">
                        <!-- List group -->
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>01/03/2015</strong> 
                                <span class="label label-danger">Pago - R$78,00</span>
                                <span class="label label-success">Saldo R$311,00</span>
                            </li>
                            <li class="list-group-item">
                                <strong>15/03/2015</strong> 
                                <span class="label label-danger">Pago - R$120,00</span>
                                <span class="label label-success">Saldo R$191,00</span>
                            </li>
                            <li class="list-group-item">
                                <strong>01/04/2015</strong> 
                                <span class="label label-danger">Pago - R$91,00</span>
                                <span class="label label-success">Saldo R$100,00</span>
                            </li>
                            <li class="list-group-item">
                                <strong>15/04/2015</strong> 
                                <span class="label label-danger">Pago - R$50,00</span>
                                <span class="label label-success">Saldo R$50,00</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-6">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><strong>Histório de Aulas</strong></div>
                    <div class="panel-body">
                        <!-- List group -->
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <strong>01/03/2015</strong> 
                                    </div>
                                    <div class="col-xs-9">
                                        <span class="label label-info">Samba - 15h30</span>
                                        <span class="label label-danger">Alunos 11</span>
                                        <span class="label label-success">Saldo R$33,00</span>
                                        <br>
                                        <span class="label label-info">Zouk - 20h00</span>
                                        <span class="label label-danger">Alunos 8</span>
                                        <span class="label label-success">Saldo R$24,00</span>    
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <strong>03/03/2015</strong> 
                                    </div>
                                    <div class="col-xs-9">
                                        <span class="label label-info">Samba - 15h30</span>
                                        <span class="label label-danger">Alunos 13</span>
                                        <span class="label label-success">Saldo R$39,00</span>
                                        <br>
                                        <span class="label label-info">Zouk - 20h00</span>
                                        <span class="label label-danger">Alunos 10</span>
                                        <span class="label label-success">Saldo R$30,00</span>    
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr>
            <div class="col-xs-12">
                <button type="button" class="btn btn-success btn-lg">Saldo Total em <?php echo date("d") . "/" . date("m") . "/" . date("Y")?> <strong>R$335,00</strong></button>
            </div>
        </div>
    </div>
@stop