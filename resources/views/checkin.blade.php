<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Check-in | Dance Sempre</title>
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://oss.maxcdn.com/semantic-ui/2.0.8/components/button.min.css">
	<link rel="stylesheet" href="https://oss.maxcdn.com/semantic-ui/2.0.8/components/form.min.css">
	<link rel="stylesheet" href="https://oss.maxcdn.com/semantic-ui/2.0.8/components/message.min.css">
	<link href="{{ asset('/css/style_checkin.css') }}" rel="stylesheet">
</head>
<body>
	@yield('content')
	<footer class="px3">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<hr>
					<img class="center-block" title="Pixel3 Solutions" title="Pixel3 Solutions" src="{{ asset('images/pixel.png') }}">
				</div>
			</div>
		</div>
	</footer>
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
</body>
</html>
