<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') Admin</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/style_login.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 top-20">
                <img class="center-block" src="{{ asset('images/dance-sempre-color.png') }}">
                @if(Session::has('flash_message'))
                    <div class="alert {{ Session::get('flash_type') }} alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

            </div>
        </div>
    </div>
    <div class="container">
    	<div class="row">
    		<div class="col-xs-12 col-md-6 col-md-offset-3">
    			@if (count($errors) > 0)
    				<div class="alert alert-danger">
    					<strong>Whoops!</strong> Houve alguns problemas.<br><br>
    					<ul>
    						@foreach ($errors->all() as $error)
    							<li>{{ $error }}</li>
    						@endforeach
    					</ul>
    				</div>
    			@endif

    			<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
    				<input type="hidden" name="_token" value="{{ csrf_token() }}">

    				<div class="form-group">
    					<label class="col-md-4 control-label">E-Mail</label>
    					<div class="col-md-6">
    						<input type="email" class="form-control" name="email" value="{{ old('email') }}">
    					</div>
    				</div>

    				<div class="form-group">
    					<label class="col-md-4 control-label">Senha</label>
    					<div class="col-md-6">
    						<input type="password" class="form-control" name="password">
    					</div>
    				</div>

    				<div class="form-group">
    					<div class="col-md-6 col-md-offset-4">
    						<div class="checkbox">
    							<label>
    								<input type="checkbox" name="remember"> Lembrar
    							</label>
    						</div>
    					</div>
    				</div>

    				<div class="form-group">
    					<div class="col-md-6 col-md-offset-4">
    						<button type="submit" class="btn btn-primary">Login</button>
    						<a class="btn btn-link" href="{{ url('/password/email') }}">Esqueceu sua senha?</a>
    					</div>
    				</div>
    			</form>
    		</div>
    	</div>
    </div>
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
