<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') Admin</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/style_login.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('flash_message'))
                    <div class="alert {{ Session::get('flash_type') }} alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

            </div>
        </div>
    </div>
    <div class="container">
    	<div class="row">
    		<div class="col-xs-12 col-md-6 col-md-offset-3 top-20">
                <img class="center-block" src="{{ asset('images/dance-sempre-color.png') }}">
    			@if (count($errors) > 0)
    				<div class="alert alert-danger">
    					<strong>Whoops!</strong> Houve alguns problemas.<br><br>
    					<ul>
    						@foreach ($errors->all() as $error)
    							<li>{{ $error }}</li>
    						@endforeach
    					</ul>
    				</div>
    			@endif

    			<a href="{{ url('facebook/authorize/user') }}" class="btn btn-primary btn-lg btn-block"><i class="fa fa-facebook-square"></i> Login com facebook</a>
    		</div>
    	</div>
    </div>
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
