@extends('pdf')
@section('title', 'Testes')
@section('head')
	<link rel="stylesheet" href="{{ asset('css/print_carteirinha.css') }}">
@endsection
@section('content')
    <div class="etiqueta text-center <?php echo $classe ?>">
    	<div id="code"><?php echo \DNS1D::getBarcodeHTML($codigo, "C128", 1, 32); ?></div>
    	<span id="dados-aluno"><?php echo $codigo ?><br/><?php echo $nome ?></span>
    </div>
@endsection