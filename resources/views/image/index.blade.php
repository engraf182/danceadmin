@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Teste Upload no Dropbox</h1>
                </div>
            </div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Erro!</strong>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                {!! Form::open(['route'=>['banners.store'], 'files'=>true]) !!}
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="control-group">
                                    <div class="controls">
                                        {!! Form::label('imgPrincipal', 'Imagem:') !!}
                                        {!! Form::file('imgPrincipal') !!}
                                        <p class="errors">{!!$errors->first('imgPrincipal')!!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    {!! Form::submit('Enviar Imagem', ['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection