<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            {!! Form::label('modalidade', 'Modalidade:') !!}
            {!! Form::text('modalidade', null, ['class'=>'form-control', 'id'=>'texto']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            {!! Form::label('info', 'Texto Adicional:') !!}
            {!! Form::text('info', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            {!! Form::label('texto', 'Texto:') !!}
            {!! Form::textarea('texto', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            {!! Form::label('slug', 'URL Amigável:') !!}
            {!! Form::text('slug', null, ['class'=>'form-control', 'id'=>'slug']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="control-group">
                    <div class="controls">
                        {!! Form::label('foto', 'Imagem:') !!}
                        {!! Form::file('foto') !!}
                    </div>{{-- /.controls --}}
                </div>{{-- /.control-group --}}
            </div>{{-- /.panel-body --}}
        </div>{{-- /.panel panel-default --}}
    </div>
    {{--<div class="col-xs-12 col-md-12">--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('info', 'Texto Adicional:') !!}--}}
            {{--{!! Form::textarea('info', null, ['class' => 'ckeditor', 'id' => 'editor']) !!}--}}
        {{--</div>--}}
    {{--</div>--}}
</div>

<hr/>
<div class="form-group">
    {!! Form::submit($text_button, ['class'=>'btn btn-primary pull-right']) !!}
</div>
