@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {{-- Breadcrumb --}}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h1>Modalidades</h1>
                <a href="{{ route('modalidades.create') }}" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-plus lg"></i></span>Adicionar modalidade</a><br/><br/>
                <div class="table-responsive">
                    {!! Form::model($modalidades, ['route'=>['modalidades.update_ord'], 'files'=>true, 'method'=>'put']) !!}
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="50">#</th>
                                    <th>Modalidade</th>
                                    <th></th>
                                </tr>
                                <!--
                                $table->increments('id');
                                $table->string('modalidade', 100);
                                $table->string('info', 150);
                                $table->timestamps();
                                -->
                            </thead>
                            <tbody>
                                @foreach($modalidades as $modalidade)
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <input type="hidden" name="id[]" value="{{ $modalidade->id }}">
                                                <input type="text" class="form-control" name="ord[{{ $modalidade->id }}]" value="{{ $modalidade->ord }}">
                                            </div>
                                        </td>
                                        <td>{{ $modalidade->modalidade }}<br/><i>{{ $modalidade->info }}</i></td>
                                        <td class="crud-button">
                                            <a class="btn btn-success" href="{{ route('modalidades.edit',['id'=>$modalidade->id]) }}"><i class="fa fa-pencil-square-o"></i></a>
                                            <a class="btn btn-danger" href="{{ route('modalidades.destroy',['id'=>$modalidade->id]) }}"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-refresh"></i></span>Atualizar dados</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

@stop