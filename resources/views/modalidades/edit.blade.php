@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Editar Modalidade</h1>
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::model($modalidade, ['route'=>['modalidades.update', $modalidade->id], 'files'=>true, 'method'=>'put']) !!}
                @include('modalidades.form', ['text_button' => 'Atualizar Dados'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script type="text/javascript">
        $(document).on('change', '#texto', function() {
            var slug = function(str) {
                var $slug = '';
                var trimmed = $.trim(str);

                $slug = trimmed
                    .replace(/[ç]/gi, 'c')
                    .replace(/[á|à|ã|â]/gi, 'a')
                    .replace(/[é|è|ê]/gi, 'e')
                    .replace(/[í|ì|ï|î]/gi, 'i')
                    .replace(/[ó|ò|õ|ô]/gi, 'o')
                    .replace(/[ú|ù|û|ü]/gi, 'u')
                    .replace(/[^a-z0-9-]/gi, '-')
                    .replace(/-+/g, '-')
                    .replace(/^-|-$/g, '');

                return $slug.toLowerCase();
            };

            $('#slug').val(slug($('#texto').val()));
        });
    </script>
@endsection