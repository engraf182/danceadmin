@extends('site')
@section('botao')
	<div class="container visible-xs" style="padding-top: 20px;">
		<div class="row">
			<div class="col-xs-5 col-xs-offset-1">
				<a class="btn btn-lg btn-danger pull-right" style="color: #fff;" href="../horarios">Horários</a>
			</div>
			<div class="col-xs-1">&nbsp;</div>
			<div class="col-xs-5">
				<a class="btn btn-lg btn-danger" style="color: #fff;" href="../valores">Valores</a>
			</div>
		</div>
	</div>
@endsection
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Pré Cadastro</h1>
                    {{-- <h1>Adicionar Painelista</h1> --}}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    @if(Session::has('flash_message'))
                        <div class="alert {{ Session::get('flash_type') }} alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                </div>
            </div>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::open(['route'=>['pre_cadastro.store']]) !!}
                    <div class="row">
                        <div class="col-xs-12 col-md-9">
                            <div class="form-group">
                                {!! Form::label('nome', 'Nome:') !!}
                                {!! Form::text('nome', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                {!! Form::label('nascimento', 'Data de Nascimento:') !!}
                                {!! Form::input('text', 'nascimento', null, ['class'=>'form-control', 'data-mask'=>'99/99/9999']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row form-cep">
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                {!! Form::label('tel_fixo', 'Tel Fixo:') !!}
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                                    {!! Form::text('tel_fixo', null, ['class'=>'form-control', 'placeholder'=>'41 3333-3333', 'data-mask'=>'99 9999-9999']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group">
                                {!! Form::label('tel_celular', 'Tel Celular:') !!}
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-mobile fa-fw"></i></span>
                                    {!! Form::text('tel_celular', null, ['class'=>'form-control', 'placeholder'=>'41 8888-8888', 'data-mask'=>'99 9999-9999']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                {!! Form::label('email', 'Email:') !!}
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                                    {!! Form::input('email', 'email', null, ['class'=>'form-control']) !!}
                                </div>
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <div class="form-group">
                                {!! Form::label('cep', 'CEP:') !!}
                                {!! Form::input('cep', 'cep', null, ['class'=>'form-control cep', 'data-mask'=>'99999999']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-8">
                            <div class="form-group">
                                {!! Form::label('endereco', 'Endereço completo:') !!}
                                {!! Form::input('endereco', 'endereco', null, ['class'=>'form-control logradouro']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <div class="form-group">
                                {!! Form::label('bairro', 'Bairro:') !!}
                                {!! Form::input('bairro', 'bairro', null, ['class'=>'form-control bairro', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group">
                        {!! Form::submit('Ok', ['class'=>'btn btn-primary pull-right']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('assets/busca_aluno/ajax_alunos_sist_antigo.js') }}"></script>
    <div class="modal fade" id="busca_aluno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Buscar</h4>
                </div>

                    <input type="hidden" name="id_menu" value="1">
                    <div class="modal-body">
                        <div class="form-group input-group">
                            <input type="text" class="form-control" name="txtnome" id="txtnome" placeholder="Consultar sistema antigo">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" onclick="getDados();">
                                    <i class="fa fa-search lg"></i>
                                </button>
                            </span>
                        </div>
                        <div id="Resultado" style="max-height: 60vh; overflow-y:auto;"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script type="text/javascript">
        $(document).on('change', '.form-cep .cep', function() {
            var cep = $(this).val(),
                formulario = $(this).closest('.form-cep'), // pega ESTE formulário
                url = 'http://viacep.com.br/ws/' + cep + '/json/';

                $.getJSON(url, function(result){
                    var logradouro = result.logradouro,
                        bairro = result.bairro,
                        localidade = result.localidade,
                        uf = result.uf,
                        endereco = logradouro + ', ' + bairro;

                    // encontra e substitui os campos DESSE formulário
                    formulario.find('.logradouro').val(logradouro);
                    formulario.find('.bairro').val(bairro);
                    formulario.find('.localidade').val(localidade);
                    formulario.find('.uf').val(uf);
                });

        });
    </script>
@endsection
