@extends('site')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-header">
				    <h1>Fotos</h1>
				</div>
			</div>
		</div>
        <div class="row">
            @foreach($albuns as $album)
                <div class="col-md-6">
                    <div class="page header">
                        <h3>{{$album->title}}</h3>
                    </div>
                    {!! $album->code !!}
                </div>
            @endforeach
        </div>
	</div>
@stop
