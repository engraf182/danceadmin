@extends('site')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-header">
				    <h1>{{ $data->titulo_pag }}</h1>
				</div>
			</div>
			<div class="col-xs-12">
				<?php echo $data->content_pagina; ?>
			</div>
		</div>
	</div>
@stop