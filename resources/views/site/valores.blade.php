@extends('site')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-header">
				  <h1>Valores</h1>
				</div>
			</div>          
        </div>          
        <div class="row">
			<div class="col-xs-12 col-md-6" id="pricing">
				<div class="panel panel-default">
					<div class="panel-body">
						<h2>Por Mensalidade</h2>
						<hr>
						<div class="row">		
                            <div class="col-xs-12">
                                    <div class="panel panel-default text-center">
                                        <div class="panel-heading">
                                            <h2 style="text-align: center">
                                                <span style="text-decoration: line-through; font-size: 15pt">De R$249,90</span><br>
                                                <span style="font-size: 15pt">Por</span> R$ <span class="price text-danger">199</span><sup class="cents">,90</sup>
                                                <br /><small>Livre mensal</small>
                                            </h2>
                                        </div>
                                        <div class="panel-body">
                                            <!-- INICIO DO BOTAO PAGSEGURO -->
                                                <a href="https://pag.ae/7UShNovsG/button" target="_blank" title="Pagar com PagSeguro"><img src="//assets.pagseguro.com.br/ps-integration-assets/botoes/pagamentos/205x30-pagar.gif" alt="Pague com PagSeguro - é rápido, grátis e seguro!" /></a>
                                            <!-- FIM DO BOTAO PAGSEGURO -->
                                        </div>
                                    </div>
                            </div><!-- ./col-xs-12 -->	
                            
						</div><!-- ./row -->

					</div><!-- /.panel-body -->
				</div><!-- /.panel -->
            </div>
            
            <div class="col-xs-12 col-md-6" id="pricing">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2>Por Créditos</h2>
                        <hr>
                        <div class="row">
                            <div class="col-xs-12">
                                    <div class="panel panel-default text-center">
                                        <div class="panel-heading">
                                            <h2 style="text-align: center">
                                                <!--<span style="text-decoration: line-through; font-size: 15pt">De R$170,00</span>--><br>
                                                <span style="font-size: 15pt">Por</span> R$ <span class="price text-danger">170</span><sup class="cents">,00</sup>
                                                <br /><small>10 Horas/Aula</small>
                                            </h2>
                                        </div>
                                        <div class="panel-body">
                                            <!-- INICIO DO BOTAO PAGSEGURO -->
                                                <a href="https://pag.ae/7UShLZYsJ/button" target="_blank" title="Pagar com PagSeguro"><img src="//assets.pagseguro.com.br/ps-integration-assets/botoes/pagamentos/205x30-pagar.gif" alt="Pague com PagSeguro - é rápido, grátis e seguro!" /></a>
                                            <!-- FIM DO BOTAO PAGSEGURO -->
                                        </div>
                                    </div>
                            </div><!-- ./col-xs-12 -->
                            
                        </div><!-- ./row -->

                    </div><!-- /.panel-body -->
                </div><!-- /.panel -->
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p class="text-center"><small>*Valores individuais.</small></p>
                <div class="col-xs-12">
                    <div class="alert alert-warning" role="alert">
                        <p>Os horários das aulas poderão ser alterados sem aviso prévio, não implicando isso em reembolso de nenhum valor.</p>
                    </div>
                </div>
            </div>
        </div>
	</div>
@stop
