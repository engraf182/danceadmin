@extends('site')
@section('head')
	<link rel="stylesheet" type="text/css" href="//oss.maxcdn.com/semantic-ui/2.1.4/semantic.min.css">
@stop
@section('js')
	<script src="//oss.maxcdn.com/semantic-ui/2.1.4/semantic.min.js"></script>
@stop
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-header">
				    <h1>Horários</h1>
				</div>



				<div class="ui link centered cards">

					{{-- Modalidade--}}
						<?php $semana[] = ''; ?>
						@foreach($modalidades as $modalidade)
							<a href="/horario/{{$modalidade[0]->slug}}" class="ui card">
								<div class="image">
									@if(empty($modalidade[0]->foto))
										<img src="images/fundo-01.jpg">
									@else
										<img src="uploads/modalidade/{{ $modalidade[0]->foto }}">
									@endif
								</div>
								<div class="content">
									<div class="header">{{ $modalidade[0]->modalidade }}</div>
									@if(!empty($modalidade[0]->info))
										<div class="description">
											{{ $modalidade[0]->info }}
										</div>
									@endif
								</div>
							</a>
						@endforeach
					{{-- Modalidade--}}

				</div>



			</div>{{-- /.col-xs-12 --}}
		</div>
	</div>
@stop
