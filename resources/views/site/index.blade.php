@extends('site')

@section('botao')
	<div class="container visible-xs" style="padding-top: 20px;">
		<div class="row">
			<div class="col-xs-5 col-xs-offset-1">
				<a class="btn btn-lg btn-danger pull-right" style="color: #fff;" href="../horarios">Horários</a>
			</div>
			<div class="col-xs-1">&nbsp;</div>
			<div class="col-xs-5">
				<a class="btn btn-lg btn-danger" style="color: #fff;" href="../valores">Valores</a>
			</div>
		</div>
	</div>
@endsection

@section('content')
		<div id="slider-wrapper" class="container">
			<section id="slider" class="row slider">
				<br>
				<div class="flex-container">
					<div class="flexslider">
						<ul class="slides">
							@foreach($banners as $banner)
								<li>
									<a href="{{ $banner->link }}" target="{{ $banner->target }}"><img src="{{ URL::asset('uploads/banners/desk') }}/{{ $banner->imgPrincipal }}" class="hidden-xs"/></a>
									<a href="{{ $banner->link }}" target="{{ $banner->target }}"><img src="{{ URL::asset('uploads/banners/mob') }}/{{ $banner->imgMobile }}" class="visible-xs"/></a>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			</section>
		</div><!-- /.container -->
		<br><br>
		<section id="noticias" class="container-fluid">

			<div class="row">
				<div class="container">
					<div class="row">
							<article class="col-xs-12 col-md-4 text-center">
								<h3>{{ $data['items'][0]->get_title() }}</h3>
								<p class="text-mute text-justify">
									<?php echo $data['items'][0]->get_description() ?>
									<hr>
								</p>
							</article><!-- ./col-xs-12 -->
							<article class="col-xs-12 col-md-4 text-center">
								<h3>{{ $data['items'][1]->get_title() }}</h3>
								<p class="text-mute text-justify">
									<?php echo $data['items'][1]->get_description() ?>
									<hr>
								</p>
							</article><!-- ./col-xs-12 -->
							<article class="col-xs-12 col-md-4 text-center">
								<h3>{{ $data['items'][2]->get_title() }}</h3>
								<p class="text-mute text-justify">
									<?php echo $data['items'][2]->get_description() ?>
									<hr>
								</p>
							</article><!-- ./col-xs-12 -->
					</div><!-- ./row -->
					<div class="row">
						<div class="col-xs-12 hidden-xs">
							<a href="http://www.dancesempre.com/blog" class="btn btn-primary btn-lg btn-block">Veja essas e mais notícias no Blog Dance Sempre!</a>
						</div>
					</div>
				</div><!-- ./container -->
			</div><!-- ./row -->
		</section><!-- /.container -->

		<section id="areas" class="container-fluid">

			<div class="row">
				<div class="container">
					<div class="row">
						<a href="http://aluno.dancesempre.com" title="Acessar área do aluno">
							<div id="area-aluno" class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2 text-center">
								<h4>Área do Aluno</h4>
								<p>
									Em breve uma área exclusiva aos nosos alunos.
								</p>
							</div><!-- ./col-xs-12 -->
						</a>
						<a href="#">
							<div id="area-professor" class="col-xs-12 col-sm-6 col-md-4 text-center">
								<h4>Área do Professor</h4>
								<p>
									Em breve uma área exclusiva aos nosos professores.
								</p>
							</div><!-- ./col-xs-12 -->
						</a>
					</div><!-- ./row -->
				</div><!-- ./container -->
			</div><!-- ./row -->

		</section><!-- /.container -->
@stop
