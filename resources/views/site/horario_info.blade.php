@extends('site')
@section('head')
	<link rel="stylesheet" type="text/css" href="//oss.maxcdn.com/semantic-ui/2.1.4/semantic.min.css">
@stop
@section('js')
	<script src="//oss.maxcdn.com/semantic-ui/2.1.4/semantic.min.js"></script>
@stop
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div style="padding-top: 20px;" class="page-header">
					<h1>
						Horários de {{ $modalidade->modalidade }}
						@if(!empty($modalidade->info))
							<br /><small class="text-white">{{ $modalidade->info }}</small>
						@endif
					</h1>
				    <h2>Escolha um horário e faça uma aula experimental gratuita!</h2>
				</div>
				<?php
					$semana = array();
					foreach ($turmas as $turm) {
						$dias_s = $turm->diasSemana;
						if ($turm->modalidades_id == $modalidade->id) {
							if (!in_array($dias_s, $semana)) {
								$semana[] = $turm->diasSemana;
							}
							$turmas_info[] = $turm;
						}
					}
				?>
				@if(empty($modalidade->foto))
					<img style="margin-bottom: 15px;" class="ui centered medium bordered image" src="../images/fundo-01.jpg">
				@else
					<img style="margin-bottom: 15px;" class="ui centered medium bordered image" src="../uploads/modalidade/{{ $modalidade->foto }}">
				@endif
				<div class="ui centered cards">
					@foreach($semana as $s)
						<div class="card">
							<div class="content">
								<div class="header">
									{{ $s }}
								</div>
								<div class="description">
									<table class="ui very basic collapsing celled table">
										@foreach($turmas_info as $m_turma)
											@if($m_turma['diasSemana'] == $s)
												<tr>
												    <td>{{ $m_turma['horarioInicio'] }} às {{ $m_turma['horarioFim'] }}</td>
												    <td>{{ $m_turma['nivel'] }}</td>
												</tr>
											@endif
										@endforeach
									</table>
								</div>
							</div>
						</div>
					@endforeach

				</div>

				<?php
					$semana = array();
					$turmas_info = array();
				?>

				<div style="clear: booth; heigth: 15px;"></div>
				<p class="text-justify">{{ $modalidade->texto }}</p>
			</div>{{-- /.col-xs-12 --}}
		</div>
		<div class="row">
			<div class="col-xs-12" style="padding-top: 20px; padding-bottom: 20px;">
				<hr>
				<h2>Outras modalidades disponíveis</h2>
			</div>
			<div class="col-xs-12">
				@foreach($modalidades as $modalid)
					@if(!empty($modalid->turmas[0]))
						@if($modalidade->id <> $modalid->id)
							<a href="../horario/{{$modalid->slug}}" style="margin: 5px 0;" class="ui red button">{{$modalid->modalidade}}</a>
						@endif
					@endif
				@endforeach
			</div>
		</div>
	</div>
@stop
