@extends('site')
@section('botao')
	<div class="container visible-xs" style="padding-top: 20px;">
		<div class="row">
			<div class="col-xs-5 col-xs-offset-1">
				<a class="btn btn-lg btn-danger pull-right" style="color: #fff;" href="../horarios">Hor�rios</a>
			</div>
			<div class="col-xs-1">&nbsp;</div>
			<div class="col-xs-5">
				<a class="btn btn-lg btn-danger" style="color: #fff;" href="../valores">Valores</a>
			</div>
		</div>
	</div>
@endsection
@section('content')
			<!-- Wrapper -->
			<div class="wrapper">

				<section id="horarios" class="container-fluid">

					<div class="row">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<div class="page-header">
										<h1>Contato</h1>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6">
									{!! Form::open(array('action' => 'SiteController@email', 'role' => 'form')) !!}
									    <div class="form-group">
									        <label for="nome">Nome</label>
									        <input type="text" class="form-control" name="nome">
									    </div>
									    <div class="form-group">
									        <label for="email">Email</label>
									        <input type="email" class="form-control" name="email">
									    </div>
									    <div class="form-group">
									    	<label class="col-xs-12 col-sm-3 control-label" for="telefone">Telefone</label>
									    	<input type="tel" class="form-control" id="telefone" data-mask="99-9999-9999" name="telefone">
									    </div>
									    <div class="form-group">
									        <label for="assunto">Assunto</label>
									        <input type="text" class="form-control" name="assunto">
									    </div>
									    <textarea class="form-control" name="msg" rows="5"></textarea>
									    <p style="margin-top: 10px;" class="text-right"><button type="submit" class="btn btn-success"><i class="fa fa-paper-plane-o"></i> Enviar</button></p>
									{!! Form::close() !!}
								</div><!-- ./col-xs-6 -->
								<div class="col-xs-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Contatos</h3>
										</div>
										<div class="panel-body">
											<p><i class="glyphicon glyphicon-earphone"></i> 41 3023-1553</p>
											<p><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:venhapara@dancesempre.com">venhapara@dancesempre.com</a></p>
											<p><i class="glyphicon glyphicon-screenshot"></i> Pra&ccedil;a Os&oacute;rio, 431, &Uacute;ltimo andar - Centro | Curitiba - PR</p>
										</div>
									</div>
								</div><!-- ./col-xs-6 -->
							</div><!-- ./row -->
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title"><i class="glyphicon glyphicon-screenshot"></i> Mapa</h3>
										</div>
										<div class="panel-body">
											<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3603.137885518661!2d-49.2818157!3d-25.433653299999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce40ad76c827b%3A0x2b8b6f09bc0285e2!2sAlameda+Dr.+Carlos+de+Carvalho%2C+708+-+Centro%2C+Matriz%2C+Curitiba+-+PR!5e0!3m2!1spt-BR!2sbr!4v1427385253480" width="100%" height="300" frameborder="0" style="border:0"></iframe>
										</div>
									</div>
								</div>
							</div>
						</div><!-- ./container -->
					</div><!-- ./row -->

				</section><!-- /.container -->

			</div><!-- /.Wrapper -->
@stop
