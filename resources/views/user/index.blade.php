@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Usuários</h1>
                <a href="{{ route('professores.create') }}" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-plus lg"></i></span>Adicionar Usuário</a><br/><br/>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th></th>
                        </tr>
                        @foreach($usuarios as $usuario)
                            <tr>
                                <td>{{ $usuario->name }}</td>
                                <td>{{ $usuario->email }}</td>
                                <td>
                                    {{--<a class="btn btn-success" href="{{ route('professores.edit',['id'=>$prof->id]) }}"><i class="fa fa-pencil-square-o"></i></a>--}}
                                    {{--<a class="btn btn-danger" href="{{ route('professores.destroy',['id'=>$prof->id]) }}"><i class="fa fa-trash-o"></i></a>--}}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
@stop