@extends('checkin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <img class="text-center" src="{{ asset('images/dance-sempre-checkin.png') }}">
            </div>
            <div class="col-xs-12">
                {{-- <h2 class="text-center"><small>Passe o código de barras da sua carteirinha</small></h2>
                <hr> --}}
                <div class="row top-percent">
                    <div class="col-xs-3 col-md-4 col-md-offset-4">
                        <form method="post" action="{{route('checkin.store')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="ui large form">
                                <div class="field">
                                    <input type="text" name="id_aluno" placeholder="Código do aluno" required="" autofocus>
                                    <input type="hidden" name="id_turma" value="{{ $id }}">
                                </div>
                            </div>
                            <button type="submit" class="ui inverted red basic button btn-block go-dance"><i class="fa fa-check-circle-o"></i> Go Dance!</button>
                            <a class="ui inverted green basic button btn-block voltar" href="/checkin" style="font-size: 12px;"><i class="fa fa-arrow-circle-o-left"></i> Voltar</a>
                        </form>
                    </div>
                </div>
            </div>{{-- /.col-xs-12 --}}
        </div>
    </div>
@stop
