@extends('checkin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <img class="text-center" src="{{ asset('images/dance-sempre-checkin.png') }}">
            </div>
            <div class="col-xs-12">
                {{-- <h2 class="text-center"><small>Passe o código de barras da sua carteirinha</small></h2>
                <hr> --}}
                <div class="row top-percent">
                    <div class="col-xs-3 col-md-6 col-md-offset-3">
                        <p class="text-center" style="font-size: 50px;"><i class="fa fa-refresh fa-spin"></i></p>
                        <p id="counter" class="text-center"></p>
                        @if(Session::has('flash_message'))
                            <div class="ui {{ Session::get('flash_type') }} message" style="font-size: 25px;"><?php echo Session::get('flash_message') ?></div>
                        @endif
                    </div>
                </div>
            </div>{{-- /.col-xs-12 --}}
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        $(function() {
            var counter = 3;
            setInterval(function() {
                $('#counter').text(counter);
                counter --;
            }, 1000);
        })
        setTimeout(function() {
            window.location.href = "/checkin";
        }, 3000);
    })
    </script>
@stop
