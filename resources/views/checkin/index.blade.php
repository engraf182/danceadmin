@extends('checkin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <img class="text-center" src="{{ asset('images/dance-sempre-checkin.png') }}">
            </div>
            <div class="col-xs-12">
                {{-- <h1 class="text-center">Modalidades Disponíveis (com início as 19h)</h1>
                <hr>
                <h2>Selecione a modalidade para fazer o check-in.<small>(Tenha em mãos sua carteirinha.)</small></h2> --}}
                <div class="row">
                    @foreach ($turmas as $turma)
                        <div class="col-xs-3">
                            <a href="{{route('checkin.check', ['id'=>$turma->id])}}" class="ui inverted red basic button btn-semantic">
                            <i class="fa fa-child"></i><br>
                            {{ $turma->modalidades['modalidade'] }}<br>
                            <small>{{ $turma->nivel }} | {{ $turma->horarioInicio }} às {{ $turma->horarioFim }}</small>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
