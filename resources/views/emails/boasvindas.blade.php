<table border="0" cellspacing="0" cellpadding="0" align="center" style="margin= 0 auto;">
    <tr>
        <td style="margin: 0 auto; padding: 0;">
            <a href="http://www.futurecom.com.br" target="_blank">
                <img src="http://aluno.dancesempre.com/images/mail/ds_topo.png" style="border: 0; display: block;" width="650" height="80" alt="www.dancesempre.com" title="www.dancesempre.com">
            </a>
        </td>
    </tr>
</table>


<table border="0" cellspacing="0" cellpadding="0" align="center" style="margin= 0 auto;">
    <tr>
        <td>
            <p>&nbsp;</p>
        </td>
    </tr>
    <tr>
        <td style="margin: 0 auto; padding: 0;">
            <h2 style="font-family: Arial, sans-serif; text-align: center; color: #3a3a3a;">
                Olá <strong>{{ $nome }}</strong>!
            </h2>
            <p style="font-family: Arial, sans-serif; text-align: center; color: #3a3a3a;">Estamos muito felizes que voc&ecirc; se juntou a n&oacute;s.</p>
            <p style="font-family: Arial, sans-serif; text-align: center; color: #3a3a3a;">
                Clique no botão abaixo para fazer login na área do aluno com o seu facebook,<br/>
                assim você pode conferir seus créditos, imprimir recibos, participar de promoções exclusivas e muito mais!
            </p>
        </td>
    </tr>
    <tr>
        <td style="margin: 0 auto; padding: 0;">
            <a href="http://aluno.dancesempre.com/facebook/authorize/{{ $id }}" target="_blank">
                <img src="http://aluno.dancesempre.com/images/mail/ds_aluno.png" style="border: 0; display: block;" width="650" height="80" alt="Login área do aluno" title="Login área do aluno">
            </a>
        </td>
    </tr>
</table>


<table border="0" cellspacing="0" cellpadding="0" align="center" style="margin= 0 auto;">
    <tr>
        <td style="margin: 0 auto; padding: 0;">
            <a href="http://www.futurecom.com.br" target="_blank">
                <img src="http://aluno.dancesempre.com/images/mail/ds_email_01.png" style="border: 0; display: block;" width="486" height="53" alt="www.futurecom.com.br" title="www.futurecom.com.br">
            </a>
        </td>
        <td style="margin: 0 auto; padding: 0;">
            <a href="http://www.facebook.com/dancesempre" target="_blank">
                <img src="http://aluno.dancesempre.com/images/mail/ds_email_02.png" style="border: 0; display: block;" width="42" height="53" alt="Facebook" title="Facebook">
            </a>
        </td>
        <td style="margin: 0 auto; padding: 0;">
            <a href="https://twitter.com/Dancesempre" target="_blank">
                <img src="http://aluno.dancesempre.com/images/mail/ds_email_03.png" style="border: 0; display: block;" width="38" height="53" alt="Twitter" title="Twitter">
            </a>
        </td>
        <td style="margin: 0 auto; padding: 0;">
            <a href="https://www.youtube.com/user/sempredancesempre#p/a" target="_blank">
                <img src="http://aluno.dancesempre.com/images/mail/ds_email_04.png" style="border: 0; display: block;" width="40" height="53" alt="Youtube" title="Youtube">
            </a>
        </td>
        <td style="margin: 0 auto; padding: 0;">
            <a href="#" target="_blank">
                <img src="http://aluno.dancesempre.com/images/mail/ds_email_05.png" style="border: 0; display: block;" width="44" height="53" alt="Instagram" title="Instagram">
            </a>
        </td>
    </tr>
</table>