@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Editar Banner</h1>
                </div>
            </div>

            <?php
                if($banner->target == "_blank"){
                    $blank = "true";
                }

                if($banner->target == "_self"){
                    $self = "true";
                }
            ?>
            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::model($banner, ['route'=>['banners.update', $banner->id], 'files'=>true, 'method'=>'put']) !!}
                    @include('banner.form', ['text_button' => 'Atualizar Banner'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection