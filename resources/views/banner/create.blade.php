@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {{-- Bradcrumb --}}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h1>Criar Banner</h1>
                </div>
            </div>
            <!--
            $table->increments('id');
            $table->integer('ordem');
            $table->string('link');
            $table->string('target');
            $table->string('nomeBanner');
            $table->string('imgPrincipal');
            $table->string('imgMobile');
            $table->timestamps();
            -->

            @include('errors.erros_form')
            <div class="col-xs-12">
                {!! Form::open(['route'=>['banners.store'], 'files'=>true]) !!}
                    @include('banner.form', ['text_button' => 'Criar Banner'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection