<div class="row">
    <div class="col-xs-12 col-md-3">
        <div class="row">
            <div class="col-xs-12 col-md-7">
                <div class="form-group">
                    {!! Form::label('ordem', 'Ordem do Banner:') !!}
                    {!! Form::input('number', 'ordem', null, ['class'=>'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-9">
        <div class="form-group">
            {!! Form::label('nomeBanner', 'Nome do Banner:') !!}
            {!! Form::text('nomeBanner', null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-md-3">
        <div class="form-group">
            {!! Form::radio('target', '_blank', null, ['class' => 'field', 'id' => 'blank']) !!}
            {!! Form::label('blank', 'Abrir em nova aba') !!}<br/>
            {!! Form::radio('target', '_self', null, ['class' => 'field', 'id' => 'self']) !!}
            {!! Form::label('self', 'Abrir na mesma página') !!}
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group">
            {!! Form::label('link', 'Link:') !!}
            {!! Form::text('link', null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="control-group">
                    <div class="controls">
                        {!! Form::label('imgPrincipal', 'Banner Desktop:') !!}
                        {!! Form::file('imgPrincipal') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="control-group">
                    <div class="controls">
                        {!! Form::label('imgMobile', 'Banner Mobile:') !!}
                        {!! Form::file('imgMobile') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr/>
<div class="form-group">
    {!! Form::submit($text_button, ['class'=>'btn btn-primary pull-right']) !!}
</div>