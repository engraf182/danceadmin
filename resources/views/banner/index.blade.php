@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {{-- Breadcrumb --}}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h1>Banners</h1>
                <a href="{{ route('banners.create') }}" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-plus lg"></i></span>Novo banner</a><br/><br/>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nome do Banner</th>
                                <th>Banner Desktop</th>
                                <th>Banner Mobile</th>
                                <th>Link</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($banners as $banner)
                                <tr>
                                    <td>{{ $banner->nomeBanner }}</td>
                                    <td><img class="thumbnail" style="max-resolution:100px; max-height:100px;" src="{{ URL::asset('uploads/banners/desk') }}/{{ $banner->imgPrincipal }}"></td>
                                    <td><img class="thumbnail" style="max-resolution:100px; max-height:100px;" src="{{ URL::asset('uploads/banners/mob') }}/{{ $banner->imgMobile }}"></td>
                                    <td>{{ $banner->link }}</td>
                                    <td>
                                        <a class="btn btn-success" href="{{ route('banners.edit',['id'=>$banner->id]) }}"><i class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-danger" href="{{ route('banners.destroy',['id'=>$banner->id]) }}"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $banners->render() !!}
            </div>
        </div>

@stop