@extends('app')

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				{{-- Bradcrumb --}}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="page-header">
					{{-- <h1>Add</h1> --}}
					<h1>Adicionar Credito para:<br/><small>{{ $aluno->nome }}</small></h1>
				</div>
			</div>
			<div class="col-xs-12 col-md-5">
				<ul class="list-group">
					<li class="list-group-item">
						<strong>Aluno:</strong> {{ $aluno->nome }}
					</li>
					<li class="list-group-item">
						<span class="badge">{{ $aluno->creditos['credito'] }}</span>
						<strong>Créditos:</strong>
					</li>
					<li class="list-group-item">
						<strong>Validade:</strong> {{ $aluno->creditos['validade'] }}
					</li>
				</ul>
			</div>
			<div class="col-xs-12 col-md-7">
				@include('errors.erros_form')
				{!! Form::open(['route'=>['financeiro.atualiza_credito', $aluno->creditos['id']], 'method'=>'put']) !!}
					<input type="hidden" name="credito_atual" value="{{ $aluno->creditos['credito'] }}">
					<div class="row">
						{{-- <div class="col-xs-12 col-md-6">
							<p><strong>Créditos</strong></p>
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn-success active">
									<input type="radio" name="credito" id="credito_1" value="1.00" autocomplete="off" checked><i class="fa fa-plus-circle"></i> 1 - R$17
								</label>
								<label class="btn btn-success">
									<input type="radio" name="credito" id="credito_3" value="10.00" autocomplete="off"><i class="fa fa-plus-circle"></i> 10 - R$170
								</label>
								<label class="btn btn-success">
									<input type="radio" name="credito" id="credito_4" value="20.00" autocomplete="off"><i class="fa fa-plus-circle"></i> 20 - R$272
								</label>
								<label class="btn btn-success">
									<input type="radio" name="credito" id="credito_5" value="40.00" autocomplete="off"><i class="fa fa-plus-circle"></i> 40 - R$476
								</label>
							</div>
						</div> --}}
						<div class="col-xs-12 col-md-4">
						    <div class="form-group">
						        {!! Form::label('credito', 'Quantidade de Créditos') !!}
						        <div class="input-group margin-bottom-sm" style="margin-top: 5px; width: 150px;">
						            <span class="input-group-addon"><i class="fa fa-plus-square"></i></span>
						            {!! Form::text('credito', null, ['class'=>'form-control', 'placeholder'=>'20.0 ou 01.0', 'data-mask'=>'99.9']) !!}
						        </div>
						    </div>
						</div>
						<div class="col-xs-12 col-md-4">
						    <div class="form-group">
						        {!! Form::label('validade', 'Dias de validade') !!}
						        <div class="input-group margin-bottom-sm" style="margin-top: 5px; width: 150px;">
						            <span class="input-group-addon"><i class="fa fa-plus-square"></i></span>
						            {!! Form::text('validade', null, ['class'=>'form-control', 'placeholder'=>'30', 'data-mask'=>'99']) !!}
						        </div>
						    </div>
						</div>
						<div class="col-xs-12 col-md-4">
							<p>Forma de Pagamento</p>
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn-warning active">
								<input type="radio" name="forma_pagamento" value="Dinheiro" id="pagamento_1" autocomplete="off" checked><i class="fa fa-money"></i> Dinheiro
								</label>
								<label class="btn btn-warning">
									<input type="radio" name="forma_pagamento" value="Cartão" id="pagamento_2" autocomplete="off"><i class="fa fa-credit-card"></i> Cartão
								</label>
							</div>
						</div>
					</div>
					<hr/>
					<div class="form-group">
						{!! Form::submit('Adicionar', ['class'=>'btn btn-primary pull-right']) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{ asset('assets/busca_aluno/ajax_alunos_sist_antigo.js') }}"></script>
	<div class="modal fade" id="busca_aluno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Buscar</h4>
				</div>
				
					<input type="hidden" name="id_menu" value="1">
					<div class="modal-body">
						<div class="form-group input-group">
							<input type="text" class="form-control" name="txtnome" id="txtnome" placeholder="Consultar sistema antigo">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button" onclick="getDados();">
									<i class="fa fa-search lg"></i>
								</button>
							</span>
						</div>
						<div id="Resultado" style="max-height: 60vh; overflow-y:auto;"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
@endsection