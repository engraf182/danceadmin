<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alunos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nome', 150);
			$table->string('endereco', 255);
			$table->string('bairro', 55);
			$table->string('tel_celular', 55);
			$table->string('tel_fixo', 55);
			$table->string('email', 150);
			$table->string('obs', 150);
			$table->integer('matricula');
			$table->date('nascimento');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alunos');
	}

}
