<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCadastrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cadastros', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nome', 150);
			$table->string('cep', 10)->nullable();
			$table->string('endereco', 255);
			$table->string('bairro', 55);
			$table->string('tel_celular', 55);
			$table->string('tel_fixo', 55)->nullable();
			$table->string('email', 150);
			$table->date('nascimento');
			$table->integer('status')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cadastros');
	}

}
