<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckinsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('checkins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('alunos_id')->unsigned();
		    $table->foreign('alunos_id')->references('id')->on('alunos')->onDelete('cascade');
		    $table->integer('turmas_id')->unsigned();
		    $table->foreign('turmas_id')->references('id')->on('turmas')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('checkins');
	}

}
