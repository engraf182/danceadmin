<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurmasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('turmas', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id');
            $table->integer('professores_id')->unsigned();
            $table->foreign('professores_id')->references('id')->on('professores')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('modalidades_id')->unsigned();
            $table->foreign('modalidades_id')->references('id')->on('modalidades')->onUpdate('cascade')->onDelete('cascade');
            $table->string('nivel');
            $table->string('horarioInicio', 5);
            $table->string('horarioFim', 5);
            $table->string('diasSemana', 120);
            $table->string('credioAula', 3);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('turmas');
	}

}
