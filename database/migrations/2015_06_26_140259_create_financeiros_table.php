<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinanceirosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('financeiros', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tipoTransacao');
			$table->string('obs');
			$table->integer('user');
			$table->string('valor');
			$table->string('formaPagamento');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('financeiros');
	}

}
