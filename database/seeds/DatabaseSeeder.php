<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Paginas;
use App\User;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('PaginasTableSeeder');
        $this->command->info('Paginas table seeded!');

        $this->call('UserTableSeeder');
        $this->command->info('User table seeded!');
	}

}

class PaginasTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Paginas::insert([
		            'id' => 1,
		            'titulo_pag' => 'Institucional',
		            'slug' => 'institucional',
		            'content_pagina' => 'Inserir Conteúdo',
		        ]);
		Paginas::insert([
		            'id' => 2,
		            'titulo_pag' => 'Serviços',
		            'slug' => 'servicos',
		            'content_pagina' => 'Inserir Conteúdo',
		        ]);
		Paginas::insert([
		            'id' => 3,
		            'titulo_pag' => 'Metodologia',
		            'slug' => 'metodologia',
		            'content_pagina' => 'Inserir Conteúdo',
		        ]);
		Paginas::insert([
		            'id' => 4,
		            'titulo_pag' => 'Estrutura',
		            'slug' => 'estrutura',
		            'content_pagina' => 'Inserir Conteúdo',
		        ]);
		Paginas::insert([
		            'id' => 5,
		            'titulo_pag' => 'Modalidades',
		            'slug' => 'modalidades',
		            'content_pagina' => 'Inserir Conteúdo',
		        ]);
		Paginas::insert([
		            'id' => 5,
		            'titulo_pag' => 'Dança dos Noivos',
		            'slug' => 'danca-dos-noivos',
		            'content_pagina' => 'Inserir Conteúdo',
		        ]);
	}

}

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
                    'name' => 'Gregory Engraf',
                    'email' => 'gregory0409@gmail.com',
                    'password' => bcrypt('engrafdrum'),
                ]);
    }
}

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
                    'name' => 'Gregory Engraf',
                    'email' => 'gregory0409@gmail.com',
                    'password' => bcrypt('engrafdrum'),
                ]);
    }
}
