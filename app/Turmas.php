<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Turmas extends Model {

    protected $fillable = ['id_prof','id_mod', 'nivel', 'horarioInicio', 'horarioFim', 'diasSemana', 'credioAula'];

    public function professores()
    {
        return $this->belongsTo('App\Professores');
    }

    public function modalidades()
    {
        return $this->belongsTo('App\Modalidades');
    }
}
