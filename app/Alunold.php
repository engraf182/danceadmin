<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Alunold extends Model {

	protected $table = 'alunos_sis_antigo';

	protected $dates = array('data_nasc');
}