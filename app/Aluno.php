<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model {

	protected $fillable = ['nome','modalidades','endereco','bairro','tel_celular','tel_fixo','email','obs','matricula','nascimento'];

	protected $dates = array('nascimento');

	public function creditos()
	{
	    return $this->hasOne('App\Creditos');
	}

}
