<?php namespace App\Events;

use App\Events\Event;
use App\Aluno;

use Illuminate\Queue\SerializesModels;

class InsertAluno extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	private $aluno;

	public function __construct(Aluno $aluno)
	{
		$this->aluno = $aluno;
	}

	public function getAluno()
	{
		return $this->aluno;
	}

}
