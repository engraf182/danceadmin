<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cadastro extends Model {

	protected $fillable = ['nome', 'cep', 'endereco', 'bairro', 'tel_celular', 'tel_fixo', 'email', 'nascimento', 'status'];

}
