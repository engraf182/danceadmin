<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('webmail', ['as' =>'webmail', 'uses'=>'SiteController@webmail']);
Route::get('alunos-antigos-busca/{termo}',['as'=>'alunos.antigos', 'uses'=>'AlunosAntigosController@buscaAjax']);
// Route::group(['domain' => 'checkin.dancesempre.com'], function(){
Route::group(['prefix'=>'checkin'], function(){
    Route::get('/',['as'=>'checkin', 'uses'=>'CheckinController@index']);
    Route::get('all',['as'=>'checkin.all', 'uses'=>'CheckinController@all']);
    Route::get('aula/{id}',['as'=>'checkin.check', 'uses'=>'CheckinController@check']);
    Route::get('retorno',['as'=>'checkin.retorno', 'uses'=>'CheckinController@retorno']);
    Route::post('store',['as'=>'checkin.store', 'uses'=>'CheckinController@store']);
});

// Route::group(['prefix'=>'aluno'], function(){
Route::group(['domain' => 'aluno.dancesempre.com'], function(){
    Route::get('/', ['as' =>'aluno', 'uses'=>'InicioController@aluno']);
    Route::get('login', ['as' =>'aluno.login', 'uses'=>'AuthSocialController@login']);
    Route::get('facebook/authorize/{id}', ['as' =>'authorize', 'uses'=>'AuthSocialController@authorize']);
    Route::get('facebook/login', ['as' =>'callback_authorize', 'uses'=>'AuthSocialController@callback_authorize']);
});
Route::group(['prefix'=>'admin'], function(){
// Route::group(['domain' => 'admin.dancesempre.com'], function(){
    Route::get('/', 'InicioController@index');
    Route::get('sistema_antigo', ['as'=>'alunos.teste', 'uses'=>'AlunoController@teste']);
    Route::get('cadastros', ['as'=>'alunos.cadastros', 'uses'=>'AlunoController@cadastros']);
    Route::group(['prefix'=>'paginas'], function(){
        Route::get('',['as'=>'paginas', 'uses'=>'PaginasController@index']);
        Route::get('create',['as'=>'paginas.create', 'uses'=>'PaginasController@create']);
        Route::post('store',['as'=>'paginas.store', 'uses'=>'PaginasController@store']);
        Route::get('{id}/destroy',['as'=>'paginas.destroy', 'uses'=>'PaginasController@destroy']);
        Route::put('{id}/update',['as'=>'paginas.update', 'uses'=>'PaginasController@update']);
        Route::get('{id}/edit',['as'=>'paginas.edit', 'uses'=>'PaginasController@edit']);
    });

    Route::group(['prefix'=>'banners', 'where'=>['id'=>'[0-9]+']], function() {
        Route::get('',['as'=>'banners', 'uses'=>'BannerController@index']);
        Route::get('create',['as'=>'banners.create', 'uses'=>'BannerController@create']);
        Route::post('store',['as'=>'banners.store', 'uses'=>'BannerController@store']);
        Route::get('{id}/destroy',['as'=>'banners.destroy', 'uses'=>'BannerController@destroy']);
        Route::get('{id}/edit',['as'=>'banners.edit', 'uses'=>'BannerController@edit']);
        Route::put('{id}/update',['as'=>'banners.update', 'uses'=>'BannerController@update']);
    });

    Route::group(['prefix'=>'album', 'where'=>['id'=>'[0-9]+']], function() {
        Route::get('',['as'=>'album', 'uses'=>'AlbumController@index']);
        Route::get('create',['as'=>'album.create', 'uses'=>'AlbumController@create']);
        Route::post('store',['as'=>'album.store', 'uses'=>'AlbumController@store']);
        Route::get('{id}/destroy',['as'=>'album.destroy', 'uses'=>'AlbumController@destroy']);
        Route::get('{id}/edit',['as'=>'album.edit', 'uses'=>'AlbumController@edit']);
        Route::put('{id}/update',['as'=>'album.update', 'uses'=>'AlbumController@update']);
    });

    Route::group(['prefix'=>'professores'], function(){
        Route::get('',['as'=>'professores', 'uses'=>'ProfessorController@index']);
        Route::get('create',['as'=>'professores.create', 'uses'=>'ProfessorController@create']);
        Route::post('store',['as'=>'professores.store', 'uses'=>'ProfessorController@store']);
        Route::get('{id}/view',['as'=>'professores.view', 'uses'=>'ProfessorController@view']);
        Route::get('{id}/destroy',['as'=>'professores.destroy', 'uses'=>'ProfessorController@destroy']);
        Route::get('{id}/edit',['as'=>'professores.edit', 'uses'=>'ProfessorController@edit']);
        Route::put('{idP/update',['as'=>'professores.update', 'uses'=>'ProfessorController@update']);
    });

    Route::group(['prefix'=>'alunos'], function(){
        Route::get('',['as'=>'alunos', 'uses'=>'AlunoController@index']);
        Route::get('create',['as'=>'alunos.create', 'uses'=>'AlunoController@create']);
        Route::post('store',['as'=>'alunos.store', 'uses'=>'AlunoController@store']);
        Route::get('{id}/destroy',['as'=>'alunos.destroy', 'uses'=>'AlunoController@destroy']);
        Route::get('{id}/edit',['as'=>'alunos.edit', 'uses'=>'AlunoController@edit']);
        Route::put('{idP/update',['as'=>'alunos.update', 'uses'=>'AlunoController@update']);
        Route::get('importar_aluno/{id}',['as'=>'alunos.create2', 'uses'=>'AlunoController@create2']);
        Route::get('{id}/insert_credit',['as'=>'alunos.insert_credit', 'uses'=>'AlunoController@add_credit']);
        Route::post('carteirinha',['as'=>'alunos.carteirinha', 'uses'=>'AlunoController@carteirinha']);
        Route::get('mail',['as'=>'alunos.mail', 'uses'=>'AlunoController@mail']);
    });

    Route::group(['prefix'=>'modalidades'], function(){
        Route::get('',['as'=>'modalidades', 'uses'=>'ModalidadesController@index']);
        Route::get('create',['as'=>'modalidades.create', 'uses'=>'ModalidadesController@create']);
        Route::post('store',['as'=>'modalidades.store', 'uses'=>'ModalidadesController@store']);
        Route::get('{id}/edit',['as'=>'modalidades.edit', 'uses'=>'ModalidadesController@edit']);
        Route::put('{id}/update',['as'=>'modalidades.update', 'uses'=>'ModalidadesController@update']);
        Route::get('{id}/destroy', ['as'=>'modalidades.destroy', 'uses'=>'ModalidadesController@destroy']);
        Route::put('update_ord',['as'=>'modalidades.update_ord', 'uses'=>'ModalidadesController@update_ord']);
    });

    Route::group(['prefix'=>'turmas'], function(){
        Route::get('',['as'=>'turmas', 'uses'=>'TurmasController@index']);
        Route::get('create',['as'=>'turmas.create', 'uses'=>'TurmasController@create']);
        Route::post('store',['as'=>'turmas.store', 'uses'=>'TurmasController@store']);
        Route::get('{id}/view',['as'=>'turmas.view', 'uses'=>'TurmasController@view']);
        Route::get('{id}/edit',['as'=>'turmas.edit', 'uses'=>'TurmasController@edit']);
        Route::put('{id}/update',['as'=>'turmas.update', 'uses'=>'TurmasController@update']);
        Route::get('{id}/destroy', ['as'=>'turmas.destroy', 'uses'=>'TurmasController@destroy']);
    });

    Route::group(['prefix'=>'financeiro'], function(){
        Route::get('',['as'=>'financeiro', 'uses'=>'FinanceiroController@index']);
        Route::post('store',['as'=>'financeiro.store', 'uses'=>'FinanceiroController@store']);
        Route::get('{id}/view',['as'=>'financeiro.view', 'uses'=>'FinanceiroController@view']);
        Route::get('{id}/edit',['as'=>'financeiro.edit', 'uses'=>'FinanceiroController@edit']);
        Route::put('{id}/atualiza_credito',['as'=>'financeiro.atualiza_credito', 'uses'=>'FinanceiroController@atualiza_credito']);
        Route::get('{id}/destroy', ['as'=>'financeiro.destroy', 'uses'=>'FinanceiroController@destroy']);
    });

    Route::group(['prefix'=>'usuarios'], function(){
       Route::get('', ['as'=>'usuarios', 'uses'=>'UsuariosController@index']);
    });
});
// Route::get('/', ['as' =>'adm', 'uses'=>'InicioController@index']);
Route::get('/', ['as' =>'home', 'uses'=>'SiteController@index']);
Route::get('home', ['as' =>'home', 'uses'=>'SiteController@index']);
Route::get('horarios', ['as' =>'home', 'uses'=>'SiteController@horarios']);
Route::get('institucional', ['as' =>'institucional', 'uses'=>'SiteController@institucional']);
Route::get('servicos', ['as' =>'servicos', 'uses'=>'SiteController@servicos']);
Route::get('metodologia', ['as' =>'metodologia', 'uses'=>'SiteController@metodologia']);
Route::get('estrutura', ['as' =>'estrutura', 'uses'=>'SiteController@estrutura']);
Route::get('modalidades', ['as' =>'site.modalidades', 'uses'=>'SiteController@modalidades']);
Route::get('danca-dos-noivos', ['as' =>'dancaNoivos', 'uses'=>'SiteController@dancaNoivos']);
Route::get('valores', ['as' =>'valores', 'uses'=>'SiteController@valores']);
Route::get('contato', ['as' =>'contato', 'uses'=>'SiteController@contato']);
Route::get('blog', ['as' =>'blog', 'uses'=>'SiteController@blog']);
Route::get('cadastro',['as'=>'pre_cadastro', 'uses'=>'SiteController@pre_cadastro']);
Route::post('store-precadastro',['as'=>'pre_cadastro.store', 'uses'=>'SiteController@store_pre']);
Route::post('mail', ['as' =>'site.mail', 'uses'=>'SiteController@email']);
Route::get('horario/{slug}', ['as' =>'horario_info', 'uses'=>'SiteController@horario_info']);
Route::get('videos', ['as' =>'videos', 'uses'=>'SiteController@videos']);
Route::get('fotos', ['as' =>'fotos', 'uses'=>'SiteController@fotos']);

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
