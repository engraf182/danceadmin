<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('Inicio'));
});

// Home > Banner
Breadcrumbs::register('banner', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Banners', route('banners'));
});

// Home > Banner > Criar Banner
Breadcrumbs::register('criar_banner', function($breadcrumbs)
{
    $breadcrumbs->parent('banner');
    $breadcrumbs->push('Criar Banner', route('banners.create'));
});

// Home > Banner > [Editar]
Breadcrumbs::register('editar_banner', function($breadcrumbs, $banner)
{
    $breadcrumbs->parent('banner');
    $breadcrumbs->push($banner->nomeBanner, route('banners.edit', $banner->id));
});





// Home > Blog
Breadcrumbs::register('blog', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Blog', route('blog'));
});

// Home > Blog > [Category]
Breadcrumbs::register('category', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('blog');
    $breadcrumbs->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Page]
Breadcrumbs::register('page', function($breadcrumbs, $page)
{
    $breadcrumbs->parent('category', $page->category);
    $breadcrumbs->push($page->title, route('page', $page->id));
});