<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpBannerRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return [
            'nomeBanner' => 'required|min:5',
            'link' => 'required',
            'imgPrincipal' => 'mimes:jpeg,bmp,png',
            'imgMobile' => 'mimes:jpeg,bmp,png'
        ];
	}

}
