<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ModalidadeRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return [
            'modalidade' => 'required|min:3',
            'slug' => 'required|unique:modalidades,slug',
            // 'info' => 'required'
        ];
	}

}
