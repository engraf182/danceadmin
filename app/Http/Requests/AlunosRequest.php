<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class AlunosRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'nome' => 'required|min:5',
            'endereco' => 'required|min:5',
            'bairro' => 'required',
            'tel_celular' => 'required',
            'tel_fixo' => 'required',
            'email' => 'unique:alunos,email'
        ];
	}

}
