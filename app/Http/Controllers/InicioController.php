<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Aluno;

class InicioController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('inicio.index');
    }

    public function aluno()
    {
    	$id = \Auth::user()->id;

    	$aluno = Aluno::where('user_id', $id)->get();
        return view('inicio.aluno', compact('aluno'));
    }

}
