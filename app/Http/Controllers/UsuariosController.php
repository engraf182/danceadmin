<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;

class UsuariosController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(User $usuario)
    {
        $usuarios = $usuario->paginate(10);
        return view('user.index', compact('usuarios'));
    }

    public function create()
    {
        return view('professores.create');
    }

}
