<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Album;
use Input;

class AlbumController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		$albuns = Album::paginate(10);
		return view('album.index', compact('albuns'));
	}
	public function create()
	{
		return view('album.create');
	}
	public function store(Request $request)
	{
		$album = new Album();
		$album->title = $request->title;
		$album->code  = $request->code;
		$album->save();

		\Session::flash('flash_message', 'Ok, album criado com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('album');
	}
	public function destroy($id)
	{
		Album::find($id)->delete();
		\Session::flash('flash_message', 'Ok, album excluído com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('album');
	}

}
