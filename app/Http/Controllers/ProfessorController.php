<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ProfessorRequest as ProfessorRequest;
use Illuminate\Database\Eloquent;
use App\Professores;

class ProfessorController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Professores $prof)
    {
        $professores = $prof->paginate(10);
        return view('professores.index', compact('professores'));
    }

    public function view($id)
    {
        $prof = Professores::find($id);
        return view('professores.view', compact('prof'));
    }

    public function create()
    {
        return view('professores.create');
    }

    public function  store(ProfessorRequest $request)
    {
        $input = $request->all();
        $input['data_nasc'] = \DateTime::createFromFormat('d/m/Y', $input['data_nasc'])->format('Y-m-d');
        Professores::create($input);

        \Session::flash('flash_message', 'Ok, pessoa adicionada com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('professores');
    }

    public function  edit($id)
    {
        $professor = Professores::find($id);
        return view('professores.edit', compact('professor'));
    }

    public function  update(ProfessorRequest $request, $id)
    {
        $professor = Professores::find($id);
        $professor['data_nasc'] = \DateTime::createFromFormat('d/m/Y', $professor['data_nasc'])->format('Y-m-d');
        $professor-> nome_prof = $request->nome_prof;
        $professor-> fone_fixo = $request->fone_fixo;
        $professor-> fone_movel = $request->fone_movel;
        $professor-> email = $request->email;
        $professor->save();

        \Session::flash('flash_message', 'Ok, pessoa adicionada com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('professores');
    }

    public function  destroy($id)
    {
        Professores::find($id)->delete();
        \Session::flash('flash_message', 'Ok, pessoa removida com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect('professores');
    }

    public function turmas()
    {
        return $this->hasMany('App\Turmas');
    }
}
