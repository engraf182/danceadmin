<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Alunold;

class AlunosAntigosController extends Controller {

	public function buscaAjax($termo)
    {
        $alunos = Alunold::where('aluno_nome', 'like', '%'.$termo.'%')->select('aluno_id as id','aluno_nome as aluno')->get();
		if ($alunos->count() > 0) {
			$tabela = "<table class=\"table table-hover\">";
			$return = "$tabela";
			foreach ($alunos as $value) {
				$return.= "<tr><td>><a href=\"importar_aluno/".$value->id."\">".$value->aluno."</a></td></tr>";
			}
			$return.="</table>";
		}else{
			$return = 'Não foram encontrados registros!';
		}
        return $return;
    }
}
