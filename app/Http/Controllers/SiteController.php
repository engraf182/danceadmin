<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Banner;
use App\Modalidades;
use App\Turmas;
use App\Paginas;
use App\Album;
use Illuminate\Http\Request;
use Feeds;
use SEO;
use App\Cadastro;
use App\Http\Requests\CadastroRequest as CadastroRequest;
use Mail;
use Input;
use Youtube;

class SiteController extends Controller {

	public function __construct()
    {

    }

    public function index()
    {
    	$feed = Feeds::make('http://blog.dancesempre.com/feed/');
        $simplePieInstance = $feed->getRawFeederObject();
        $data = array(
            'title'     => $simplePieInstance->get_title(),
            'permalink' => $simplePieInstance->get_permalink(),
            'items'     => $simplePieInstance->get_items(),
        );
    	$banners = Banner::all();
        return view('site.index', compact('banners', 'data'));
    }

	public function videos()
	{
		$list = Youtube::getPlaylistsByChannelId('UCEWHPFNilsT0IfQfutVzsag');
		$video = Youtube::getVideoInfo('rie-hPVJ7Sw');
		dd($video);
	}

	public function fotos()
	{
		$albuns = Album::all();
		return view('site.fotos', compact('albuns'));
	}

    public function horarios(Modalidades $mod, Turmas $turma)
    {
        $modalidades = $mod->join('turmas', 'modalidades.id', '=', 'turmas.modalidades_id')
			->select('modalidades.*')
			->get()
			->groupBy('id');
        $turmas = $turma
                    ->orderBy('diasSemana', 'asc')
                    ->orderBy('horarioInicio', 'asc')
                    ->get();
        return view('site.horarios', compact('modalidades', 'turmas', 'turmas_semana'));
    }

    public function horario_info($slug, Modalidades $mod, Turmas $turma)
    {
        $modalidade = $mod->where('slug', $slug)->first();
        $modalidades = $mod->all();
        $turmas = $turma
                    ->where('modalidades_id', $modalidade->id)
                    ->orderBy('diasSemana', 'asc')
                    ->orderBy('horarioInicio', 'asc')
                    ->get();
        return view('site.horario_info', compact('modalidade', 'turmas', 'turmas_semana', 'modalidades'));
    }

    public function institucional(Paginas $pag)
    {
        $data = $pag->find(1);
        return view('site.page', compact('data'));
    }

    public function servicos(Paginas $pag)
    {
        $data = $pag->find(2);
        return view('site.page', compact('data'));
    }

    public function metodologia(Paginas $pag)
    {
        $data = $pag->find(3);
        return view('site.page', compact('data'));
    }

    public function estrutura(Paginas $pag)
    {
        $data = $pag->find(4);
        return view('site.page', compact('data'));
    }

    public function modalidades(Paginas $pag)
    {
        $data = $pag->find(5);
        return view('site.page', compact('data'));
    }

    public function dancaNoivos(Paginas $pag)
    {
        $data = $pag->find(6);
		$buttons = false;
        return view('site.page', compact('data', 'buttons'));
    }

    public function valores()
    {
        return view('site.valores');
    }

    public function blog()
    {
        return redirect('http://blog.dancesempre.com');
    }

    public function webmail()
    {
        return redirect('https://www.zoho.com/mail/login.html');
    }
    public function contato()
    {
        return view('site.contato');
    }

    public function pre_cadastro()
    {
        return view('site.cadastro');
    }

    public function store_pre(CadastroRequest $request)
    {
        $input = $request->all();
        $input['nascimento'] = \DateTime::createFromFormat('d/m/Y', $input['nascimento'])->format('Y-m-d');

        $cadastro = Cadastro::create($input);

        \Session::flash('flash_message', 'Ok, pré cadastro realizado com sucesso!');
        \Session::flash('flash_type', 'alert-success');

        return \Redirect::to('cadastro');
    }

    public function email()
    {

        \Mail::send('emails.contato',
            array(
                'nome' => \Request::input('nome'),
                'email' => \Request::input('email'),
                'telefone' => \Request::input('telefone'),
                'assunto' => \Request::input('assunto'),
                'user_message' => \Request::input('msg')
            ), function($message)
        {
            $message->from(\Request::input('email'), \Request::input('nome'));
            $message->to('venhapara@dancesempre.com')->subject(\Request::input('assunto'));
        });


        \Session::flash('flash_message', 'Ok, contato com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return \Redirect::route('contato');

    }

}
