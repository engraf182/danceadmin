<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Checkin;
use App\Aluno;
use App\Turmas;
use App\Creditos;
use App\Financeiro;
use Carbon\Carbon;

class CheckinController extends Controller {

	public function __construct()
	{
	   $this->middleware('auth');
	}

	public function index()
	{
	    // $banners = $banner->paginate(10);
	    $hora = Carbon::now('America/Sao_Paulo')->format('H');
	    $agora = Carbon::now('America/Sao_Paulo')->addMinutes(17)->format('Hi');
	    $uma_hora_atras = Carbon::now('America/Sao_Paulo')->subMinutes(15)->format('Hi');
	    $min  = Carbon::now('America/Sao_Paulo')->format('i');
	    $dia  = Carbon::now('America/Sao_Paulo')->format('l');

	    switch ($dia) {
	    	case 'Sunday':
	    		$dia_br = "Domingo";
	    		break;
	    	case 'Monday':
	    		$dia_br =  "Segunda";
	    		break;
	    	case 'Tuesday':
	    		$dia_br =  "Terça";
	    		break;
	    	case 'Wednesday':
	    		$dia_br =  "Quarta";
	    		break;
	    	case 'Thursday':
	    		$dia_br =  "Quinta";
	    		break;
	    	case 'Friday':
	    		$dia_br =  "Sexta";
	    		break;
	    	case 'Saturday':
	    		$dia_br =  "Sábado";
	    		break;
	    }

	    $turm = Turmas::all();
	    $turmas = "";
	    foreach ($turm as $value) {
	    	$d = explode(',', $value->diasSemana);
	    	$inicio = explode('h', $value->horarioInicio);
	    	$inicio = $inicio[0].$inicio[1];
	    	if (in_array($dia_br, $d)) {

	    		if ($inicio >= $uma_hora_atras) {
	    			$turmas[] = $value;
	    		}

	    	}
	    }
	    return view('checkin.index', compact('turmas'));
	}

	public function check($id)
	{
	    return view('checkin.check', compact('id'));
	}

	public function retorno()
	{
	    return view('checkin.return');
	}

	public function store(Request $request)
	{
		$turma = $request->id_turma;
		$aluno = $request->id_aluno;

		$data_aluno = Aluno::find($aluno);
		$cred_id = $data_aluno->creditos['id'];
		if ($data_aluno == null) {
			\Session::flash('flash_message', 'Aluno não encontrado.');
        	\Session::flash('flash_type', 'red');
		}else{
			$data_turma = Turmas::find($turma);
			$credito_aula = $data_turma->credioAula;
			$aluno_nome = $data_aluno->nome;
			$aluno_credito = $data_aluno->creditos['credito'];
			$registra = $aluno_credito - $credito_aula;
			if ($registra >= 0) {
				$check = new Checkin();
				$check-> alunos_id = $aluno;
				$check-> turmas_id = $turma;
				$check->save();

				$cred = Creditos::find($cred_id);
				$cred->credito = $registra;
				$cred->updated_at = Carbon::now();
				$cred->save();

				$financeiro= new Financeiro();
				$financeiro->tipoTransacao = 1;
				$financeiro->obs = "Aluno|" . $data_turma->modalidades['modalidade'];
				$financeiro->user = $aluno;
				$financeiro->valor = $credito_aula;
				$financeiro->formaPagamento = "Checkin Dance Sempre";
				$financeiro->save();
				\Session::flash('flash_message', 'Olá <strong>' . $aluno_nome . '</strong>' . ', let\'s dance!');
	        	\Session::flash('flash_type', 'green');
			}else{
				\Session::flash('flash_message', 'Olá <strong>' . $aluno_nome . '</strong>' . ', não foi possível registrar sua presença.');
	        	\Session::flash('flash_type', 'red');
			}
		}
		
	    return view('checkin.return');
	}

	public function all()
	{
	    $turmas = Turmas::all();
	    return view('checkin.index', compact('turmas'));
	}

}
