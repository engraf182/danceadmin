<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreditoRequest;
use App\Http\Controllers\Controller;
use App\Creditos;
use App\Financeiro;
use App\Aluno;
use Carbon\Carbon;

use Illuminate\Http\Request;

class FinanceiroController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }


	public function atualiza_credito($id, CreditoRequest $request, Creditos $credito)
	{
		$cred_atual = $request->credito_atual;
		$cred_novo  = $request->credito;
		$cred_final = $cred_atual+$cred_novo;
		$val  		= $request->validade;
		$validade   = Carbon::today()->addDays($val);
	    $up_credito = Creditos::find($id);
        $up_credito->credito  = $cred_final;
        $up_credito->validade = $validade;
        
        // $up_credito->credito = 10.00;
	    
	    if ($up_credito->save()) {
	    	\Session::flash('flash_message', 'Ok, crédito adicionado com sucesso!');
		    \Session::flash('flash_type', 'alert-success');
		    return redirect()->route('alunos');
	    }else{
	    	\Session::flash('flash_message', 'Erro, não foi possível adicionar o crédito.');
		    \Session::flash('flash_type', 'alert-danger');
		    return redirect()->route('alunos');
	    }
	}
}
