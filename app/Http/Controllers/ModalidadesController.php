<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modalidades;
use App\Http\Requests\ModalidadeRequest;
use Input;
use Illuminate\Contracts\Filesystem\Filesystem;

class ModalidadesController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Modalidades $mod)
    {
        // $modalidades = $mod->paginate(10);
        $modalidades = Modalidades::all();
        return view('modalidades.index', compact('modalidades'));
    }

    public function create()
    {
        return view('modalidades.create');
    }

    public function store(ModalidadeRequest $request, Filesystem $fileStorage)
    {
        $fileDesk = Input::file('foto');
        if ($fileDesk->isValid()) {
            $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
            $img = 'modalidade'.rand(11111,99999).'.'.$extension; // renameing image
            $fileStorage->put("modalidade/$img", file_get_contents($fileDesk));
        }

        $modalidade = new Modalidades();
        $modalidade-> modalidade = $request->modalidade;
        $modalidade-> info       = $request->info;
        $modalidade-> slug       = $request->slug;
        $modalidade-> texto      = $request->texto;
        $modalidade-> foto       = $img;
        $modalidade->save();

        \Session::flash('flash_message', 'Ok, modalidade adicionada');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('modalidades');
    }

    public function  edit($id)
    {
        $modalidade = Modalidades::find($id);
        return view('modalidades.edit', compact('modalidade'));
    }

    public function  update(Request $request, $id, Filesystem $fileStorage)
    {
        $modalidade = Modalidades::find($id);

        $fileDesk = Input::file('foto');
        if ($fileDesk == !null) {
            $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
            $img = 'modalidade'.rand(11111,99999).'.'.$extension; // renameing image
            $fileStorage->put("modalidade/$img", file_get_contents($fileDesk));
            $modalidade->foto = $img;
        }

        $modalidade->modalidade = $request->modalidade;
        $modalidade->info  = $request->info;
        $modalidade->texto = $request->texto;
        $modalidade->save();

        \Session::flash('flash_message', 'Ok, dados alterados');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('modalidades');
    }

    public function  destroy($id)
    {
        Modalidades::find($id)->delete();

        \Session::flash('flash_message', 'Ok, modalidade removida');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('modalidades');
    }

    public function update_ord(Request $request)
    {
        $ordens = $request->ord;
        $ids    = $request->id;
        foreach ($ids as $key => $id) {
            $ord = $ordens[$id];
            $modalidade = Modalidades::find($id);
            $modalidade-> ord = $ord;
            $modalidade->save();
        }


        \Session::flash('flash_message', 'Ok, ordens de modalidade atualizado.');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('modalidades');
    }

}
