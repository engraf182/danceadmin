<?php namespace App\Http\Controllers;
namespace App\Http\Controllers;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Events\InsertAluno;
use App\Aluno;
use App\Alunold;
use Illuminate\Database\Eloquent;
use Illuminate\Http\Request;
use App\Http\Requests\AlunosRequest as AlunosRequest;
use PDF;
use DNS1D;
use Mail;
use App\Cadastro;

class AlunoController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }

	public function cadastros()
	{
		$alunos = Cadastro::all();
		return view('alunos.cadastro', compact('alunos'));
	}
    public function mail()
    {
        $aluno = Aluno::find(1);
        $email = $aluno->email;
        \Mail::send('emails.boasvindas',
            array(
                'nome' => $aluno->nome,
                'email' => $aluno->email,
                'dados' => $aluno,
                'assunto' => "Bem Vindo à Dance Sempre"
            ), function($message) use ($email)
        {
            $message->from('venhapara@dancesempre.com', 'Dance Sempre');
            $message->to('greg@pixel3.solutions')->subject('Bem Vindo à Dance Sempre');
        });
        return view('emails.boasvindas');
    }

    public function index(Aluno $aluno)
    {
        $alunos = $aluno->all();
        return view('alunos.index', compact('alunos'));
    }

    public function carteirinha(Request $request)
    {
        $parameterr = array();
        $parameter['classe'] = $request->classe;
        $parameter['nome']   = $request->nome;
        $parameter['codigo'] = $request->codigo;
        $pdf = PDF::loadView('pdf.carteirinha', $parameter);
        return $pdf->stream("carteirinha.pdf");
    }

    public function add_credit($id)
    {
        $aluno = Aluno::find($id);
        return view('financeiro.add_credito', compact('aluno'));
    }

    public function teste()
    {
        $alunos = Alunold::all();
        return view('alunos.teste', compact('alunos'));
    }

    public function create()
    {
        return view('alunos.create');
    }

    public function  store(AlunosRequest $request)
    {
        $input = $request->all();
        $input['nascimento'] = \DateTime::createFromFormat('d/m/Y', $input['nascimento'])->format('Y-m-d');

        $aluno = Aluno::create($input);

        \Session::flash('flash_message', 'Ok, Aluno adicionado com sucesso!');
        \Session::flash('flash_type', 'alert-success');

        $res = \Event::fire(new InsertAluno($aluno));

        return \Redirect::to('alunos/'.$aluno->id.'/insert_credit');
    }

    public function create2($id)
    {
        $aluno = Alunold::where('aluno_id', '=', $id)->first();
		
        return view('alunos.create2', compact('aluno'));
    }

}
