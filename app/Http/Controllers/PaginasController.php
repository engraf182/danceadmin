<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Paginas;
use App\Http\Requests\PaginasRequest;

class PaginasController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Paginas $pag)
    {
        $paginas = $pag->paginate(10);
        return view('paginas.index', compact('paginas'));
    }

    public function create()
    {
        return view('paginas.create');
    }

    public function store(PaginasRequest $request)
    {
        $input = $request->all();
        Paginas::create($input);

        \Session::flash('flash_message', 'Ok, página adicionada');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('paginas');
    }

    public function  edit($id)
    {
        $pagina = Paginas::find($id);
        return view('paginas.edit', compact('pagina'));
    }

    public function  update(PaginasRequest $request, $id)
    {
        $pagina = Paginas::find($id);
        $pagina-> titulo_pag = $request->titulo_pag;
        $pagina-> content_pagina = $request->content_pagina;
        $pagina->save();

        \Session::flash('flash_message', 'Ok, dados alterados');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('paginas');
    }

    public function  destroy($id)
    {
        Paginas::find($id)->delete();
        \Session::flash('flash_message', 'Ok, página removida');
        \Session::flash('flash_type', 'alert-success');
        return redirect('paginas');
    }

}
