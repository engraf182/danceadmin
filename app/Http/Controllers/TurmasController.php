<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\TurmasRequest;
use App\Turmas;
use App\Professores;
use App\Modalidades;

class TurmasController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Turmas $turma)
    {
        $turmas = $turma->all();
        return view('turmas.index', compact('turmas'));
    }

    public function create()
    {
        $professores = Professores::lists('nome_prof', 'id');
        $modalidades = Modalidades::lists('modalidade', 'id');
        return view('turmas.create', compact('professores', 'modalidades'));
    }

    public function  store(TurmasRequest $request)
    {
        $turma = new Turmas();
        $turma-> modalidades_id = $request->modalidade;
        $turma-> professores_id = $request->professor;
        $turma-> nivel = $request->nivel;
        $turma-> horarioInicio = $request->horarioInicio;
        $turma-> horarioFim = $request->horarioFim;
        $turma-> credioAula = $request->credioAula;
        $turma-> diasSemana = implode(',', $request->diasSemana);
        $turma->save();

//        $input = $request->all();
//        Turmas::create($input);

        \Session::flash('flash_message', 'Ok, turma adicionada com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('turmas');
    }

    public function view($id)
    {
        $turma = Turmas::find($id);
        return view('turmas.view', compact('turma'));
    }

    public function  edit($id)
    {
        $turma = Turmas::find($id);
        $professores = Professores::lists('nome_prof', 'id');
        $modalidades = Modalidades::lists('modalidade', 'id');
        return view('turmas.edit', compact('professores', 'modalidades', 'turma'));
    }

    public function  update(TurmasRequest $request, $id)
    {
        $turma = Turmas::find($id);
        $turma-> modalidades_id = $request->modalidade;
        $turma-> professores_id = $request->professor;
        $turma-> nivel = $request->nivel;
        $turma-> horarioInicio = $request->horarioInicio;
        $turma-> horarioFim = $request->horarioFim;
        $turma-> credioAula = $request->credioAula;
        $turma-> diasSemana = implode(',', $request->dias);
        $turma->save();
        \Session::flash('flash_message', 'Ok, turma alterada com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('turmas');
    }

    public function destroy($id)
    {
        $find = Turmas::find($id);
        $find->delete();
        \Session::flash('flash_message', 'Ok, turma removida com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect(route('turmas'));
    }
}
