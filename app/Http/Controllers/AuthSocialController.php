<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use OAuth;
use Illuminate\Http\Request;
use SocialNorm\Exceptions\ApplicationRejectedException;
use SocialNorm\Exceptions\InvalidAuthorizationCodeException;
use Defender;
use DB;
use App\Aluno;

class AuthSocialController extends Controller {

	public function login()
	{
		return view('auth.login_face');
	}

	public function authorize($id)
	{
		\Session::flash('flash_id', $id);
		return OAuth::authorize('facebook');
	}

	public function callback_authorize()
	{
		$id_aluno = \Session::get('flash_id');
		if ($id_aluno <> 'user') {
			OAuth::login('facebook', function($user, $userDetails){
			    $user->email = $userDetails->email;
			    $user->name = $userDetails->full_name;
			    $user->avatar = $userDetails->avatar;
			    $user->save();

			    if (!Defender::roleExists('alunos')) {
			    	Defender::createRole('alunos');
			    }
			    $grupo = Defender::findRole('alunos');
			    $user_group = DB::table('role_user')
			                    ->where('user_id', $user->id)
			                    ->where('role_id', $grupo->id)
			                    ->first();
			    $teste = Defender::hasRole('alunos');
			    if ($user_group == null) {
			    	$user->attachRole($grupo);
			    }
			    $id_aluno = \Session::get('flash_id');
			    $aluno = Aluno::find($id_aluno);
			    $aluno->user_id = $user->id;
			    $aluno->save();
			});
		}else{
			try {
			        OAuth::login('facebook');
			    } catch (ApplicationRejectedException $e) {
			        // User rejected application
			    } catch (InvalidAuthorizationCodeException $e) {
			        // Authorization was attempted with invalid
			        // code,likely forgery attempt
			    }

			    // Current user is now available via Auth facade
			    $user = \Auth::user();
		}
		return redirect()->route('aluno');
	}

}
