<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\BannerRequest;
use App\Http\Requests\UpBannerRequest;
use App\Banner;
use Input;
use Illuminate\Contracts\Filesystem\Filesystem;
class BannerController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Banner $banner)
    {
        //$banners = Banner::all();
        $banners = $banner->paginate(10);
        return view('banner.index', compact('banners'));
    }

    public function create()
    {
        return view('banner.create');
    }

    public function store(BannerRequest $request, Filesystem $fileStorage)
    {
        $fileDesk = Input::file('imgPrincipal');
        $fileMob  = Input::file('imgMobile');
        if ($fileDesk->isValid()) {
            $extension = Input::file('imgPrincipal')->getClientOriginalExtension(); // getting image extension
            $desk = 'desk'.rand(11111,99999).'.'.$extension; // renameing image
            $fileStorage->put("banners/desk/$desk", file_get_contents($fileDesk));
        }
        if ($fileMob->isValid()) {
            $extension = $fileMob->getClientOriginalExtension();
            $mob = 'mob'.rand(11111,99999).'.'.$extension;
            $fileStorage->put("banners/mob/$mob", file_get_contents($fileMob));
        }
        $banner = new Banner();
        $banner-> nomeBanner = $request->nomeBanner;
        $banner-> link = $request->link;
        $banner-> ordem = $request->ordem;
        $banner-> target = $request->target;
        $banner-> imgMobile = $mob;
        $banner-> imgPrincipal = $desk;
        $banner->save();

        \Session::flash('flash_message', 'Ok, banner criado com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('banners');
    }

    public function edit($id)
    {
        $banner = Banner::find($id);
        return view('banner.edit', compact('banner'));
    }

    public function update($id, UpBannerRequest $request, Filesystem $fileStorage)
    {
        $fileDesk = Input::file('imgPrincipal');
        $fileMob  = Input::file('imgMobile');

        $banner = Banner::find($id);
        $banner->nomeBanner = $request->nomeBanner;
        $banner->link = $request->link;
        $banner->ordem = $request->ordem;
        $banner->target = $request->target;

        if($fileDesk == !null){
            $extension = Input::file('imgPrincipal')->getClientOriginalExtension(); // getting image extension
            $desk = 'desk'.rand(11111,99999).'.'.$extension; // renameing image
            $fileStorage->put("banners/desk/$desk", file_get_contents($fileDesk));
            if($fileStorage->exists("banners/desk/$banner->imgPrincipal")) {
                $fileStorage->delete("banners/desk/$banner->imgPrincipal");
            }
            $banner->imgPrincipal = $desk;
        }

        if ($fileMob == !null) {
            $extension = $fileMob->getClientOriginalExtension();
            $mob = 'mob'.rand(11111,99999).'.'.$extension;
            $fileStorage->put("banners/mob/$mob", file_get_contents($fileMob));
            if($fileStorage->exists("banners/mob/$banner->imgMobile")) {
                $fileStorage->delete("banners/mob/$banner->imgMobile");
            }
            $banner->imgMobile = $mob;
        }
        $banner->save();
        \Session::flash('flash_message', 'Ok, banner atualizado com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect()->route('banners');
    }

    public function destroy($id, Filesystem $fileStorage)
    {
        $dados = Banner::find($id);
        Banner::find($id)->delete();
        if($fileStorage->exists("banners/desk/$dados->imgPrincipal")) {
            $fileStorage->delete("banners/desk/$dados->imgPrincipal");
        }
        if($fileStorage->exists("banners/mob/$dados->imgMobile")) {
            $fileStorage->delete("banners/mob/$dados->imgMobile");
        }

        \Session::flash('flash_message', 'Ok, banner excluído com sucesso!');
        \Session::flash('flash_type', 'alert-success');
        return redirect('banners');
    }


}
