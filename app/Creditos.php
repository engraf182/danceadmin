<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Creditos extends Model {

	protected $fillable = ['aluno_id','aluno_id','credito','validade'];

	public function aluno()
    {
        return $this->belongsTo('App\Aluno');
    }

}
