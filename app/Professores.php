<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Professores extends Model {

    protected $fillable = ['nome_prof','data_nasc', 'fone_fixo', 'fone_movel', 'email'];

    protected $dates = array('data_nasc');

    public function turmas()
    {
        // return $this->hasMany('App\Turmas');
        return $this->hasOne('App\Turmas');
    }
}
