<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Paginas extends Model {

    protected $fillable = ['titulo_pag','content'];

}
