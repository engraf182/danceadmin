<?php namespace App\Handlers\Events;

use App\Events\InsertAluno;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Aluno;

class EmailBoasVindas {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{		
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  InsertAluno  $event
	 * @return void
	 */
	public function handle(InsertAluno $event)
	{
		$id = $event->getAluno()->id;
		$aluno = Aluno::find($id);
		$email = $aluno->email;
		\Mail::send('emails.boasvindas',
            array(
                'nome' => $aluno->nome,
                'id'   => $id,
            ), function($message) use ($email)
        {
            $message->from('venhapara@dancesempre.com', 'Dance Sempre');
            $message->to($email)->subject('Bem Vindo à Dance Sempre');
        });
	}

}
