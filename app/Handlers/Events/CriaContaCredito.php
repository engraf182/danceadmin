<?php namespace App\Handlers\Events;

use App\Events\InsertAluno;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Creditos;
use Carbon\Carbon;

class CriaContaCredito {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  InsertAluno  $event
	 * @return void
	 */
	public function handle(InsertAluno $event)
	{
		$aluno_id = $event->getAluno()->id;
		$cc_credito = new Creditos();
		$cc_credito-> aluno_id = $aluno_id;
		$cc_credito-> validade = Carbon::now();
		$cc_credito->save();
	}

}
