<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Financeiro extends Model {

	protected $fillable = ['tipoTransacao','obs','user','valor','formaPagamento'];

}
